import pathlib

import markdown
from bs4 import BeautifulSoup

input_folder = pathlib.Path("..") / 'конспект'
output_folder = pathlib.Path("..") / 'html_output'
table_style = 'border-collapse: collapse; width: 100%; border-style: solid; border-color: rgba(0, 0, 0, 0.1); height: 132.53125px;'


def coverter(path_input: pathlib.Path, path_output: pathlib.Path):
    with open(path_input / 'README.md', 'r') as f:
        text_md = f.read()

    text_html = markdown.markdown(text_md, extensions=['markdown.extensions.tables', 'fenced_code'])
    edit_text_html = html_edit_table(text_html)
    path_output.mkdir(parents=True, exist_ok=True)
    with open(path_output / 'index.html', 'w') as f:
        f.write(edit_text_html)


def html_edit_table(text_html):
    try:
        soup = BeautifulSoup(text_html, 'html.parser')
        for table in soup.find_all('table'):
            table['style'] = table_style
            table['border'] = '1'
            thead = table.find('thead')
            if thead and thead.tr:
                for th in thead.tr.find_all('th'):
                    text = th.get_text()
                    h4_text = BeautifulSoup(f"<h4>{text}</h4>", "xml")
                    th.string.replace_with(h4_text)
                    th.name = "td"
                table.find('tbody').insert(1, thead.tr)
                thead.decompose()
        return str(soup)
    except Exception as e:
        print(f'Exception: {e}\n{text_html}')
        return text_html


def foreach_folder(root_dir: pathlib.Path):
    for folder_1 in root_dir.rglob('*'):
        if folder_1.is_dir():
            for folder_2 in folder_1.rglob('*'):
                if folder_2.is_dir():
                    write_convert_file(folder_1.name, folder_2.name)


def write_convert_file(path1, path2):
    path_input = input_folder / path1 / path2
    path_output = output_folder / path1 / path2
    coverter(path_input, path_output)


if __name__ == '__main__':
    foreach_folder(input_folder)