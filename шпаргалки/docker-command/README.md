# Команды Docker

# (Раздел А)

## Помощь

```shell
docker SECTION --help
```

### Пример

```shell
docker --help
```

```shell
docker image --help
```

# (Раздел Б)

## Авторизация

```shell
docker login [OPTIONS] [SERVER] [flags]
```

### Пример

```shell
docker login registry-ci.delta.sbrf.ru
Username: TYZ-123
Password: ???????
```

```shell
docker login --username TYZ-123 --password ??????? registry-ci.delta.sbrf.ru
```

## Выход из учетной записи

```shell
docker logout [SERVER] [flags]
```

### Пример

```shell
docker logout registry-ci.delta.sbrf.ru
```

# (Раздел В)

## Запустить контейнер

```shell
docker container run [OPTIONS] IMAGE
```

(выделить, что слово `container` не обязательное)

### Пример

```shell
docker container run busybox
```

```shell
docker run busybox
```

## Посмотреть список работающих контейнеров

### Пример

```shell
docker container ls
CONTAINER ID   IMAGE     COMMAND       CREATED         STATUS                     PORTS                NAMES
b1d92061d50c   busybox   "sleep 100"   4 seconds ago   Up 3 seconds               0.0.0.0:80->80/tcp   my_container
```

## Посмотреть список всех контейнеров

### Пример

```shell
docker container ls --all
CONTAINER ID   IMAGE     COMMAND       CREATED         STATUS                     PORTS                NAMES
b1d92061d50c   busybox   "sleep 100"   4 seconds ago   Up 3 seconds               0.0.0.0:80->80/tcp   my_container
75476f58701a   busybox   "sleep 100"   3 minutes ago   Exited (0) 2 minutes ago                        other_container
```

|Название|Пример|Объяснение|
|:---:|:---:|---|
| `CONTAINER ID` | `b1d92061d50c` | Уникальная хеш-сумма контейнера |
| `IMAGE` | `busybox` | Имя базового Docker-образа |
| `COMMAND` | `"sleep 100"` | Стартовая команда в контейнере |
| `CREATED` | `4 seconds ago` | Время, когда был создан контейнер |
| `STATUS` | `Up 3 seconds` | Статус работы контейнера |
| `PORTS` | `0.0.0.0:80->80/tcp` | Опубликованные порты |
| `NAMES` | `my_container` | Имя контейнера |

## Остановить контейнер

```shell
docker container stop [OPTIONS] CONTAINER [CONTAINER...]
```

(выделить, что слово `container` не обязательное)

(При необходимости `[CONTAINER...]` можно убрать)

### Пример

```shell
docker container stop my_container
```

## Запустить ранее остановленный контейнер

```shell
docker container start [OPTIONS] CONTAINER [CONTAINER...]
```

(выделить, что слово `container` не обязательное)

(При необходимости `[CONTAINER...]` можно убрать)

### Пример

```shell
docker container start my_container
```

## Перезапустить контейнер

```shell
docker container restart [OPTIONS] CONTAINER [CONTAINER...]
```

(выделить, что слово `container` не обязательное)

(При необходимости `[CONTAINER...]` можно убрать)

### Пример

```shell
docker container restart my_container
```

## Удалить контейнер

```shell
docker container rm [OPTIONS] CONTAINER [CONTAINER...]
```

(При необходимости `[CONTAINER...]` можно убрать)

### Пример

```shell
docker container rm my_container other_container
```

## Удалить все остановленные контейнеры

```shell
docker container prune [OPTIONS]
```

### Пример

```shell
docker container prune
```

## Выполнить команду внутри запущенного контейнера

```shell
docker container exec [OPTIONS] CONTAINER COMMAND [ARG...]
```

### Пример

```shell
docker container exec my_container ls -l
```

## Скопировать файлы из контейнера на компьютер

```shell
docker container cp [OPTIONS] CONTAINER:SRC_PATH DEST_PATH
```

(выделить, что слово `container` не обязательное)

### Пример

```shell
docker container cp my_container:/work/out.log out.log
```

## Скопировать файлы с компьютера в контейнер

```shell
docker container cp [OPTIONS] SRC_PATH CONTAINER:DEST_PATH
```

(выделить, что слово `container` не обязательное)

### Пример

```shell
docker container cp ./config my_container:/work/config
```

## Показать лог из контейнера

```shell
docker container logs [OPTIONS] CONTAINER
```

(выделить, что слово `container` не обязательное)

### Пример

```shell
docker container logs my_container
```

# (Раздел Г)

## Подключить порт из контейнера на компьютер запуска

```shell
docker container run --publish CONTIANER_PORT:HOST_PORT IMAGE
```

### Пример

```shell
docker container run --publish 8080:8080 busybox
```

## Смонтировать файл или каталог

```shell
docker container run --volume HOST_FILE:CONTAINER_FILE IMAGE
```

### Пример

```shell
docker container run --volume ./config:/app/config/ busybox
```

## Добавить переменную окружения

```shell
docker container run --env KEY=VALUE IMAGE
```

### Пример

```shell
docker container run --env POSTGRES_PASSWORD=my_pass postgres:15
```

## Запустить контейнер в фоновом режиме

```shell
docker container run --detach IMAGE
```

### Пример

```shell
docker container run --detach busybox
```

## Явно задать имя контейнеру

```shell
docker container run --name CONTAINER_NAME IMAGE
```

### Пример

```shell
docker container run --name my_container busybox
```

## Удалить контейнер автоматически после остановки

```shell
docker container run --rm IMAGE
```

### Пример

```shell
docker container run --rm busybox
```

## Подключиться внутрь контейнера

```shell
docker container run -it IMAGE
```

### Пример

```shell
docker container run -it busybox
```

## (Раздел Д)

## Посмотреть список образов

### Пример

```shell
docker image ls

REPOSITORY              TAG     IMAGE ID      CREATED       SIZE
alpine                  3.17    b2aa39c304c2  3 months ago  7.05MB
registry.k8s.io/busybox latest  e7d168d7db45  8 years ago   2.43MB
```

|   Название   |     Пример     | Объяснение                      |
|:------------:|:--------------:|---------------------------------|
| `REPOSITORY` |    `alpine`    | Хост и имя образа               |
|    `TAG`     |    `latest`    | Версия/тег образа               |
|  `IMAGE ID`  | `b2aa39c304c2` | Уникальный идентификатор образа |
|  `CREATED`   | `3 months ago` | Время создания образа           |
|    `SIZE`    |    `7.05MB`    | Размер образа                   |

## Собрать образ из Dockerfile

```shell
docker image build [OPTIONS] PATH
```

(выделить, что слово `image` не обязательное)

### Пример

```shell
docker image build --tag my_app:1 .
```

```shell
docker build --tag my_app:1 .
```

## Создать псевдоним образа

```shell
docker image tag SOURCE_IMAGE TARGET_IMAGE
```

(выделить, что слово `image` не обязательное)

### Пример

```shell
docker image tag my_app:1 other_app:1
```

## Удалить образ

```shell
docker image rm [OPTIONS] IMAGE [IMAGE...]
```

(При необходимости `[IMAGE...]` можно убрать)

### Пример

```shell
docker image rm my_app:1
```

## Удалить все образы, не привязанные к контейнерам

```shell
docker image prune [OPTIONS]
```

### Пример

```shell
docker image prune --force
```

## Сохранить образ в файл

```shell
docker image save --output FILE_NAME IMAGE [IMAGE...]
```

(выделить, что слово `image` не обязательное)

### Пример

```shell
docker image save --output my_app.tar my_app:1
```

## Загрузить образ из файла

```shell
docker image load --input FILE_NAME
```

(выделить, что слово `image` не обязательное)

### Пример

```shell
docker image load --input my_app.tar
```

## Загрузить образ из удалённого хранилища

```shell
docker image pull [OPTIONS] NAME[:TAG|@DIGEST]
```

(выделить, что слово `image` не обязательное)

### Пример

```shell
docker image pull registry.k8s.io/busybox:latest
```

## Отправить образ в удалённое хранилище

```shell
docker image push [OPTIONS] NAME[:TAG]
```

(выделить, что слово `image` не обязательное)

```shell
docker image push docker.io/my_account/my_app:1
```