# Заголовок

Команды Kubernetes и OpenShift

# Содержание

## Выполнить любую команду над Kubernetes или OpenShift

```shell
kubectl COMMAND
```

```shell
oc COMMAND
```

### Пример

```shell
kubectl --help
```

```shell
oc --help
```

## Помощь

```shell
kubectl SECTION --help
```

```shell
oc SECTION --help
```

### Пример

```shell
kubectl apply --help
```

```shell
oc apply --help
```

## Авторизация в OpenShift

```shell
oc login --token=SHA_TOKEN --server=SERVER
```

### Пример

```shell
oc login --token=sha256~123 --server=https://example:6443
```

## Создание и обновление ресурсов

```shell
kubectl apply --filename SOURCE
```

```shell
kubectl apply -f SOURCE
```

### Пример

```shell
kubectl apply --filename pod.yaml
```

```shell
kubectl apply -f ./dir/
```

```shell
kubectl apply -f https://example.com/pod.yaml
```

## Посмотреть список ресурсов

```shell
kubectl get TYPE
```

### Пример

```shell
kubectl get pod
```

```shell
kubectl get deployment
```

## Поиск ресурсов по имени

```shell
kubectl get TYPE NAME
```

### Пример

```shell
kubectl get pod my-pod
```

```shell
kubectl get deployment my-deploy
```

## Поиск ресурсов с помощью labels

```shell
kubectl get TYPE --selector KEY=VALUE
```

```shell
kubectl get TYPE -l KEY=VALUE
```

### Пример

```shell
kubectl get pod --selector app=my-app
```

```shell
kubectl get pod -l app=my-app
```

## Вывести информацию о ресурсе в специальный формат

```shell
kubectl get TYPE --output FORMAT
```

```shell
kubectl get TYPE -o FORMAT
```

### Пример

```shell
kubectl get pod --output yaml
```

```shell
kubectl get pod my-pod -o json
```

## Фильтрация информации с помощью JSONPath

```shell
kubectl get TYPE --output jsonpath=TEMPLATE
```

```shell
kubectl get TYPE -o jsonpath=TEMPLATE
```

### Пример

```shell
kubectl get pod --output jsonpath='{.items[*]}'
```

```shell
kubectl get pod my-pod -o jsonpath='{.spec}'
```

## Сравнить состояние кластера с состоянием, если применить манифест

```shell
kubectl diff --filename SOURCE
```

### Пример

```shell
kubectl diff --filename ./new-manifest.yaml
```

## Удалить ресурсы с помощью манифеста

```shell
kubectl delete --filename SOURCE
```

### Пример

```shell
kubectl delete --filename pod.yaml
```

## Удалить ресурсы по имени

```shell
kubectl delete TYPE NAME
```

### Пример

```shell
kubectl delete pod my-pod
```

## Удалить ресурсы по label

```shell
kubectl delete TYPE --selector KEY=VALUE
```

### Пример

```shell
kubectl delete pod --selector app=my-app
```

## Вывести логи Pod'а

```shell
kubectl logs POD
```

### Пример

```shell
kubectl logs my-pod
```

```shell
kubectl logs --selector app=my-app
```

## Вывести логи Pod'а в режиме реального времени

```shell
kubectl logs --follow POD
```

```shell
kubectl logs -f POD
```

### Пример

```shell
kubectl logs --follow my-pod
```

```shell
kubectl logs -f --selector app=my-app
```

## Вывести логи контейнера

```shell
kubectl logs POD --container CONTAINER
```

```shell
kubectl logs POD -c CONTAINER
```

### Пример

```shell
kubectl logs my-pod --container my-cont
```

```shell
kubectl logs --selector app=my-app -c my-cont
```