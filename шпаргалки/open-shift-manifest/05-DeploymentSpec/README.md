# Заголовок

Manifest DeploymentSpec

# Содержание

### DeploymentSpec

| Название                  | Описание                                                                                               | Ссылка / пример              | Обязательно |
|---------------------------|--------------------------------------------------------------------------------------------------------|------------------------------|-------------|
| `selector`                | Label selector, для поиска Pod'ов, запущенных в этом Deployment. Необходим для корректного обновления. | link:`LabelSelector`         | Да          |
|                           | Должен совпадать с `Deployment` -> `template` -> `metadata`.                                           |                              |             |
| `template`                | Спецификация Pod, который будет развернут в Deployment                                                 | link:`PodTemplateSpec`       | Да          |
| `replicas`                | Количество Pod                                                                                         | example:`1` (по умолчанию)   |             |
| `strategy`                | Стратегия обновления Pod                                                                               | link:`DeploymentStrategy`    |             |
| `revisionHistoryLimit`    | Число ревизий, к которым можно вернуться                                                               | example:`10` (по умолчанию)  |             |
| `progressDeadlineSeconds` | Количество секунд, которое даётся на развертывание                                                     | example:`600` (по умолчанию) |             |
| `minReadySeconds`         | Задержка, после которой Pod должен стать готовым к работе                                              | example:`0` (по умолчанию)   |             |

### DeploymentStrategy

| Название        | Описание                                                     | Ссылка / пример                        | Обязательно |
|-----------------|--------------------------------------------------------------|----------------------------------------|-------------|
| `type`          | Стратегия обновления                                         | example:`RollingUpdate` (по умолчанию) |             |
|                 |                                                              | example:`Recreate`                     |             |
| `rollingUpdate` | Дополнительные параметры, задаются если `type=RollingUpdate` | link:`RollingUpdateDeployment`         |             |

### RollingUpdateDeployment

| Название         | Описание                                                       | Ссылка / пример              | Обязательно |
|------------------|----------------------------------------------------------------|------------------------------|-------------|
| `maxSurge`       | Количество Pod новой версии, запущенные сверх нормы `replicas` | example:`25%` (по умолчанию) |             |
|                  |                                                                | example:`5`                  |             |
| `maxUnavailable` | Количество Pod, недоступных во время обновления                | example:`25%` (по умолчанию) |             |
|                  |                                                                | example:`5`                  |             |

### Пример

```yaml
kind: Deployment
apiVersion: apps/v1
metadata:
  name: busybox
spec:
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
        - name: busybox-cont
          image: busybox
```

```yaml
kind: Deployment
apiVersion: apps/v1
metadata:
  name: busybox
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 25%
  revisionHistoryLimit: 10
  progressDeadlineSeconds: 600
  minReadySeconds: 0
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
        - name: busybox-cont
          image: busybox
```