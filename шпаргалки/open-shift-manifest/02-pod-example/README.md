# Заголовок

Manifest PodSpec

# Содержание

## (Пример 1)

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: busybox
spec:
  containers:
    - name: busybox-cont
      image: busybox
```

## (Пример 2)

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: my-app
spec:
  restartPolicy: OnFailure
  securityContext:
    runAsNonRoot: true
  initContainers:
    - name: init-cont
      image: busybox
  containers:
    - name: my-container
      image: my-web-server:latest
      ports:
        - name: http-port
          containerPort: 8080
      livenessProbe:
        httpGet:
          port: http-port
      envFrom:
        - configMapRef:
            name: my-config-map-env
      volumeMounts:
        - name: application-yaml-volume
          mountPath: /app/config/
  volumes:
    - name: application-yaml-volume
      configMap:
        name: my-config-map-file
```