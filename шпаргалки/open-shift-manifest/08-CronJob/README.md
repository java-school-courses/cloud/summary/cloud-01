# Заголовок

Manifest CronJobSpec

# Содержание

### CronJobSpec

| Название                     | Описание                                                                              | Ссылка / пример                | Обязательно |
|------------------------------|---------------------------------------------------------------------------------------|--------------------------------|-------------|
| `jobTemplate`                | Спецификация `Job`                                                                    | link:`JobTemplateSpec`         | Да          |
| `schedule`                   | Расписание в формате Cron                                                             | example:`* * * * *`            | Да          |
| `concurrencyPolicy`          | Политика запуска нового `Job`, когда предыдущий ещё не завершился                     | example:`Allow` (по умолчанию) |             |
|                              |                                                                                       | example:`Forbid`               |             |
|                              |                                                                                       | example:`Replace`              |             |
| `startingDeadlineSeconds`    | Разрешенная задержка начала `Job`.                                                    | example:`600`                  |             |
|                              | Если `Job` по какой-то причине пропустит это время, то переводится в статус `Failed`. |                                |             |
| `suspend`                    | Приостановить запуск `Job`                                                            | example:`false` (по умолчанию) |             |
| `successfulJobsHistoryLimit` | Размер истории успешных запусков                                                      | example:`3` (по умолчанию)     |             |
| `failedJobsHistoryLimit`     | Размер истории неуспешных запусков                                                    | example:`1` (по умолчанию)     |             |

### JobTemplateSpec

| Название   | Описание     | Ссылка / пример        | Обязательно |
|------------|--------------|------------------------|-------------|
| `metadata` | Метаданные   | example:`name: my-job` |             |
| `spec`     | Спецификация | link:`JobSpec`         | Да          |

### Пример

```yaml
kind: CronJob
apiVersion: batch/v1
metadata:
  name: backup
spec:
  schedule: "* * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
            - name: backup-cont
              image: my-backup:1
```

```yaml
kind: CronJob
apiVersion: batch/v1
metadata:
  name: backup
spec:
  concurrencyPolicy: Allow
  startingDeadlineSeconds: 600
  suspend: false
  successfulJobsHistoryLimit: 3
  failedJobsHistoryLimit: 1
  schedule: "@monthly"
  jobTemplate:
    metadata:
      name: my-job
    spec:
      template:
        spec:
          containers:
            - name: backup-cont
              image: my-backup:1
```