# Заголовок

Manifest ConfigMap

# Содержание

### ConfigMap

| Название                 | Описание                                               | Ссылка / пример                   | Обязательно |
|--------------------------|--------------------------------------------------------|-----------------------------------|-------------|
| `kind`                   | Тип объекта                                            | example:`ConfigMap`               | Да          |
| `apiVersion`             | Версия манифеста                                       | example:`v1`                      | Да          |
| `metadata`               | Метаданные                                             | example:`name: my-config-map`     | Да          |
| `data`                   | Массив `Ключ / Значение`                               | example:`HOST: example.com`       |             |
| `binaryData`             | Массив `Ключ / Значение`. Значение в формате `base64`. | example:`sample.txt.gz: ODA4MA==` |             |
| `immutable`              | Запретить изменение данных                             | example:`false` (по умолчанию)    |             |

### Пример

```yaml
kind: ConfigMap
apiVersion: v1
metadata:
  name: my-config-map
data:
  HOST: example.com
  PORT: 8080
```

```yaml
kind: ConfigMap
apiVersion: v1
metadata:
  name: my-config-map-files
data:
  application.properties: |
    server.port=8080
binaryData:
  sample.txt.gz: ODA4MA==
immutable: true
```