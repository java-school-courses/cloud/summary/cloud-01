# Заголовок

Manifest ServiceSpec

# Содержание

### ServiceSpec

| Название         | Описание                                                    | Ссылка / пример                    | Массив | Обязательно |
|------------------|-------------------------------------------------------------|------------------------------------|--------|-------------|
| `selector`       | Label selector, для поиска Pod'ов, на которые пойдёт трафик | example:`name: my-app`             |        |             |
| `ports`          | Связывание портов Pod'а с портами `Service`                 | link:`ServicePort`                 | Да     | Да          |
| `type`           | Каким способом можно получить доступ к `Service`            | example:`ClusterIP` (по умолчанию) |        |             |
|                  |                                                             | example:`NodePort`                 |        |             |
|                  |                                                             | example:`LoadBalancer`             |        |             |
|                  |                                                             | example:`ExternalName`             |        |             |

### ServicePort

| Название       | Описание                                                             | Ссылка / пример              | Обязательно |
|----------------|----------------------------------------------------------------------|------------------------------|-------------|
| `name`         | Название связки портов                                               | example:`my-app-api`         |             |
| `port`         | Внешний порт `Service`                                               | example:`9090`               | Да          |
| `targetPort`   | Порт, на который отвечает `Pod`                                      | example:`my-http`            |             |
| `protocol`     | Протокол транспортного уровня OSI                                    | example:`TCP` (по умолчанию) |             |
|                |                                                                      | example:`UDP`                |             |
|                |                                                                      | example:`SCTP`               |             |
| `nodePort`     | Порт, опубликованный на узле кластера, чтобы обратиться к `Service`. | example:`30000`              |             |
|                | Используется в типах `NodePort` и `LoadBalancer`.                    |                              |             |
| `appProtocol ` | Протокол уровня приложения OSI                                       | example:`http`               |             |
|                |                                                                      | example:`gRPC`               |             |

### Пример

```yaml
kind: Service
apiVersion: v1
metadata:
  name: my-service
spec:
  selector:
    app: my-app
  ports:
    - name: my-app-api
      targetPort: my-http
      port: 9090
```

```yaml
kind: Service
apiVersion: v1
metadata:
  name: my-service
spec:
  type: ClusterIP
  selector:
    app: my-app
  ports:
    - name: my-app-api
      targetPort: my-http
      port: 9090
      protocol: TCP
      appProtocol: http
```