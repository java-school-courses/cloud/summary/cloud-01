# Заголовок

Manifest Secret

# Содержание

### Secret

| Название     | Описание                                               | Ссылка / пример                | Обязательно |
|--------------|--------------------------------------------------------|--------------------------------|-------------|
| `kind`       | Тип объекта                                            | example:`Secret`               | Да          |
| `apiVersion` | Версия манифеста                                       | example:`v1`                   | Да          |
| `metadata`   | Метаданные                                             | example:`name: my-secret`      | Да          |
| `data`       | Массив `Ключ / Значение`. Значение в формате `base64`. | example:`cert.jsk: ODA4MA==`   |             |
| `binaryData` | Массив `Ключ / Значение`                               | example:`PASS: changeit`       |             |
| `immutable`  | Запретить изменение данных                             | example:`false` (по умолчанию) |             |
| `type`       | Подсказка для программной обработки секретных данных   | example:`kubernetes.io/tls`    |             |

### Пример

```yaml
kind: Secret
apiVersion: v1
metadata:
  name: db-pass
data:
  POSTGRES_PASSWORD: cGFzcw==
```

```yaml
kind: Secret
apiVersion: v1
metadata:
  name: secret-tls
data:
  tls.crt: cGFzcw==
  tls.key: ODA4MA==
type: kubernetes.io/tls
```

```yaml
kind: Secret
apiVersion: v1
metadata:
  name: my-secret
data:
  cert.jsk: ODA4MA==
binaryData:
  username: admin
  password: changeit
immutable: true
```