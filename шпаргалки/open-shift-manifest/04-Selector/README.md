# Заголовок

Manifest Selector

# Содержание

### PodTemplateSpec

| Название   | Описание                                           | Ссылка / пример      | Обязательно |
|------------|----------------------------------------------------|----------------------|-------------|
| `metadata` | Массив `Ключ / Значение` для поиска Pod            | example:`app:my-app` | Да          |
|            | Должен совпадать с `selector` управляющего объекта |                      |             |
| `spec`     | Спецификация Pod                                   | link:`PodSpec`       | Да          |

### LabelSelector

| Название           | Описание                                                     | Ссылка / пример                 | Массив | Обязательно                                 |
|--------------------|--------------------------------------------------------------|---------------------------------|--------|---------------------------------------------|
| `matchLabels`      | Массив `Ключ / Значение` для поиска Pod                      | example:`app:my-app`            |        | Либо `matchLabels`, либо `matchExpressions` |
| `matchExpressions` | Массив `Ключ / Значения / Оператор сравнения` для поиска Pod | link:`LabelSelectorRequirement` | Да     | Либо `matchLabels`, либо `matchExpressions` |

### LabelSelectorRequirement

| Название   | Описание           | Ссылка / пример               | Массив | Обязательно |
|------------|--------------------|-------------------------------|--------|-------------|
| `key`      | Ключ               | example:`app`                 |        | Да          |
| `values`   | Значения           | example:`[my-app, other-app]` | Да     | Да          |
| `operator` | Оператор сравнения | example:`In`                  |        | Да          |
|            |                    | example:`NotIn`               |        |             |
|            |                    | example:`Exists`              |        |             |
|            |                    | example:`DoesNotExist`        |        |             |

### Пример

```yaml
kind: Deployment
apiVersion: apps/v1
metadata:
  name: busybox
spec:
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
        - name: busybox-cont
          image: busybox
```

```yaml
kind: Deployment
apiVersion: apps/v1
metadata:
  name: busybox
spec:
  selector:
    matchExpressions:
      - key: app
        values: [ my-app ]
        operator: In
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
        - name: busybox-cont
          image: busybox
```