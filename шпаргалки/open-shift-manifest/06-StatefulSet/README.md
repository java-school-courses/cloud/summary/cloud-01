# Заголовок

Manifest StatefulSetSpec

# Содержание

## (Раздел 01)

### StatefulSetSpec

| Название                  | Описание                                                                                                | Ссылка / пример                       | Обязательно |
|---------------------------|---------------------------------------------------------------------------------------------------------|---------------------------------------|-------------|
| `selector`                | Label selector, для поиска Pod'ов, запущенных в этом StatefulSet. Необходим для корректного обновления. | link:`LabelSelector`                  | Да          |
|                           | Должен совпадать с `StatefulSet` -> `template` -> `metadata`.                                           |                                       |             |
| `template`                | Спецификация Pod, который будет развернут в `StatefulSet`                                               | link:`PodTemplateSpec`                | Да          |
| `serviceName`             | Поддомен DNS, который определяет адрес доступа к `StatefulSet`                                          | example:`my-app`                      | Да          |
| `replicas`                | Количество Pod                                                                                          | example:`1` (по умолчанию)            |             |
| `volumeClaimTemplates`    | Подключение постоянного хранилища к `StatefulSet`                                                       | link:`PersistentVolumeClaim`          |             |
| `updateStrategy`          | Стратегия обновления Pod                                                                                | link:`StatefulSetUpdateStrategy`      |             |
| `podManagementPolicy`     | Последовательность создания и удаления Pod                                                              | example:`OrderedReady` (по умолчанию) |             |
|                           |                                                                                                         | example:`Parallel`                    |             |
| `progressDeadlineSeconds` | Количество секунд, которое даётся на развертывание                                                      | example:`600` (по умолчанию)          |             |
| `minReadySeconds`         | Задержка, после которой Pod должен стать готовым к работе                                               | example:`0` (по умолчанию)            |             |

## (Раздел 02)

### PersistentVolumeClaim

| Название   | Описание     | Ссылка / пример                  | Обязательно |
|------------|--------------|----------------------------------|-------------|
| `metadata` | Метаданные   | example:`name: disk-ssd`         |             |
| `spec`     | Спецификация | link:`PersistentVolumeClaimSpec` |             |

### PersistentVolumeClaimSpec

| Название           | Описание                          | Ссылка / пример                     | Массив | Обязательно |
|--------------------|-----------------------------------|-------------------------------------|--------|-------------|
| `selector`         | Label selector, для поиска volume | link:`LabelSelector`                |        |             |
| `volumeName`       | Ссылка на PersistentVolume        | example:`my-volume`                 |        |             |
| `resources`        | Запрашиваемые ресурсы             | link:`ResourceRequirements`         |        |             |
| `accessModes`      | Режим доступа                     | example:`ReadOnlyMany`              | Да     |             |
|                    |                                   | example:`ReadWriteOnce`             |        |             |
|                    |                                   | example:`ReadWriteMany`             |        |             |
| `storageClassName` | Ссылка на `StorageClass`          | example:`local-storage`             |        |             |
| `volumeMode`       | Тип volume                        | example:`Filesystem` (по умолчанию) |        |             |
|                    |                                   | example:`Block`                     |        |             |

## (Раздел 03)

### StatefulSetUpdateStrategy

| Название        | Описание                                                     | Ссылка / пример                         | Обязательно |
|-----------------|--------------------------------------------------------------|-----------------------------------------|-------------|
| `type`          | Стратегия обновления                                         | example:`RollingUpdate` (по умолчанию)  |             |
|                 |                                                              | example:`OnDelete`                      |             |
| `rollingUpdate` | Дополнительные параметры, задаются если `type=RollingUpdate` | link:`RollingUpdateStatefulSetStrategy` |             |

### RollingUpdateStatefulSetStrategy

| Название         | Описание                                               | Ссылка / пример              | Обязательно |
|------------------|--------------------------------------------------------|------------------------------|-------------|
| `partition`      | Порядковый номер Pod, с которого начинается обновление | example:`0` (по умолчанию)   |             |
| `maxUnavailable` | Количество Pod, недоступных во время обновления        | example:`25%` (по умолчанию) |             |
|                  |                                                        | example:`5`                  |             |

## (Раздел 04)

### Пример

```yaml
kind: StatefulSet
apiVersion: apps/v1
metadata:
  name: db
spec:
  serviceName: ""
  selector:
    matchLabels:
      app: postgres
  template:
    metadata:
      labels:
        app: postgres
    spec:
      containers:
        - name: db-cont
          image: postgres
```

```yaml
kind: StatefulSet
apiVersion: apps/v1
metadata:
  name: db
spec:
  serviceName: "database"
  updateStrategy:
    type: OnDelete
  podManagementPolicy: Parallel
  selector:
    matchLabels:
      app: postgres
  template:
    metadata:
      labels:
        app: postgres
    spec:
      containers:
        - name: db-cont
          image: postgres
          volumeMounts:
            - name: disk-ssd
              mountPath: /var/lib/postgresql/data
  volumeClaimTemplates:
    - metadata:
        name: disk-ssd
      spec:
        accessModes: [ "ReadWriteOnce" ]
        storageClassName: "my-disk"
```