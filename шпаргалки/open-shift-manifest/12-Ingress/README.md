# Заголовок

Manifest IngressSpec

# Содержание

### IngressSpec

| Название           | Описание                                                                 | Ссылка / пример        | Массив | Обязательно                         |
|--------------------|--------------------------------------------------------------------------|------------------------|--------|-------------------------------------|
| `rules`            | Правила перенаправления внешнего трафика `Ingress` на внутренние ресурсы | link:`IngressRule`     | Да     | Либо `rules`, либо `defaultBackend` |
| `defaultBackend`   | Куда перенаправлять трафик по умолчанию                                  | link:`IngressBackend`  |        | Либо `rules`, либо `defaultBackend` |
| `ingressClassName` | Ссылка на `IngressClass`, который балансирует запросы                    | example:`my-nginx`     |        |                                     |
| `tls`              | Настройки шифрования трафика                                             | link:`IngressTls`      | Да     |                                     |

### IngressRule

| Название       | Описание                        | Ссылка / пример             | Обязательно |
|----------------|---------------------------------|-----------------------------|-------------|
| `host`         | Доменное имя                    | example:`example.com`       |             |
| `http`         | Правила перенаправления трафика | link:`HttpIngressRuleValue` |             |

### HttpIngressRuleValue

| Название   | Описание                        | Ссылка / пример        | Массив | Обязательно |
|------------|---------------------------------|------------------------|--------|-------------|
| `paths`    | Правила перенаправления трафика | link:`HttpIngressPath` | Да     |             |

### HttpIngressPath

| Название   | Описание                                    | Ссылка / пример                  | Обязательно |
|------------|---------------------------------------------|----------------------------------|-------------|
| `backend`  | Куда перенаправлять трафик                  | link:`IngressBackend`            | Да          |
| `pathType` | Каким образом сопоставлять адрес            | example:`Prefix`                 | Да          |
|            |                                             | example:`Exact`                  |             |
|            |                                             | example:`ImplementationSpecific` |             |
| `path`     | Адрес, на который будет реагировать правило | example:`/api/v1`                | Да          |

### IngressBackend

| Название   | Описание                                      | Ссылка / пример                  | Обязательно                     |
|------------|-----------------------------------------------|----------------------------------|---------------------------------|
| `service`  | Ссылка на `Service`, который принимает трафик | link:`IngressServiceBackend`     | Либо `service`, либо `resource` |
| `resource` | Ссылка на ресурс, который принимает трафик    | link:`TypedLocalObjectReference` | Либо `service`, либо `resource` |

### IngressServiceBackend

| Название | Описание                                         | Ссылка / пример           | Обязательно |
|----------|--------------------------------------------------|---------------------------|-------------|
| `name`   | Имя объекта `Service`                            | example:`my-service`      | Да          |
| `port`   | Порт `Service`, на который перенаправляем трафик | link:`ServiceBackendPort` | Да          |

### ServiceBackendPort

| Название | Описание                 | Ссылка / пример      | Обязательно                |
|----------|--------------------------|----------------------|----------------------------|
| `name`   | Литеральная запись порта | example:`my-app-api` | Либо `name`, либо `number` |
| `number` | Цифровая запись порта    | example:`9090`       | Либо `name`, либо `number` |

### TypedLocalObjectReference

| Название   | Описание           | Ссылка / пример           | Обязательно |
|------------|--------------------|---------------------------|-------------|
| `kind`     | Тип ресурса        | example:`StorageBucket`   | Да          |
| `apiGroup` | API группа ресурса | example:`k8s.example.com` |             |
| `name`     | Название ресурса   | example:`icon-assets`     | Да          |

### IngressTls

| Название     | Описание                                             | Ссылка / пример           | Массив | Обязательно |
|--------------|------------------------------------------------------|---------------------------|--------|-------------|
| `hosts`      | Доменные имена из TLS сертификата                    | example:`[ example.com ]` | Да     |             |
| `secretName` | Ссылка на `Secret` с закрытым и открытым TLS ключами | example:`secret-tls`      |        |             |

### Пример

```yaml
kind: Ingress
apiVersion: networking.k8s.io/v1
metadata:
  name: default-ingress
spec:
  defaultBackend:
    service:
      name: my-service
      port:
        name: my-app-api
```

```yaml
kind: Ingress
apiVersion: networking.k8s.io/v1
metadata:
  name: ingress-to-storage
spec:
  defaultBackend:
    resource:
      kind: StorageBucket
      apiGroup: k8s.example.com
      name: static-assets
```

```yaml
kind: Ingress
apiVersion: networking.k8s.io/v1
metadata:
  name: my-ingress
spec:
  ingressClassName: my-nginx
  tls:
    - hosts: [ example.com ]
      secretName: secret-tls
  rules:
    - host: example.com
      http:
        paths:
          - pathType: Prefix
            path: /api/v1
            backend:
              service:
                name: my-service
                port:
                  name: my-app-api
```