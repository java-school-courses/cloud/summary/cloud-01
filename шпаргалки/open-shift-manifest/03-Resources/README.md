# Заголовок

Manifest Resources

# Содержание

### ResourceRequirements

| Название   | Описание                      | Ссылка / пример      | Обязательно |
|------------|-------------------------------|----------------------|-------------|
| `limits`   | Максимально возможные ресурсы | link:`Resources`     |             |
| `requests` | Запросить ресурсы             | link:`Resources`     |             |

### Resources

| Название | Описание           | Ссылка / пример         | Обязательно |
|----------|--------------------|-------------------------|-------------|
| `memory` | Оперативная память | example:`123Mi`         |             |
|          |                    | example:`128974848000m` |             |
|          |                    | example:`129M`          |             |
|          |                    | example:`129e6`         |             |
|          |                    | example:`128974848`     |             |
| `cpu`    | Такты процессора   | example:`0.5`           |             |
|          |                    | example:`500m`          |             |

### Пример

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: busybox
spec:
  containers:
    - name: busybox-cont
      image: busybox
      resources:
        requests:
          memory: 129M
          cpu: 1
        limits:
          memory: 512Mi
          cpu: 1000m
```