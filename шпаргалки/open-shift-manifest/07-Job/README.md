# Заголовок

Manifest JobSpec

# Содержание

### JobSpec

| Название                  | Описание                                                                                               | Ссылка / пример                       | Обязательно |
|---------------------------|--------------------------------------------------------------------------------------------------------|---------------------------------------|-------------|
| `template`                | Спецификация Pod, который будет развернут в `Job`                                                      | link:`PodTemplateSpec`                | Да          |
| `parallelism`             | Количество Pod                                                                                         | example:`1` (по умолчанию)            |             |
| `completions`             | Количество успешных выполнений Pod, прежде чем перевести `Job` в статус `Complete`                     | example:`1` (по умолчанию)            |             |
| `completionMode`          | Алгоритм вычисления успешности `Job`                                                                   | example:`NonIndexed` (по умолчанию)   |             |
|                           |                                                                                                        | example:`Indexed`                     |             |
| `backoffLimit`            | Количество неуспешных запусков Pod, прежде чем перевести `Job` в статус `Failed`                       | example:`6` (по умолчанию)            |             |
| `activeDeadlineSeconds`   | Количество секунд, которое даётся на выполнение                                                        | example:`600`                         |             |
| `ttlSecondsAfterFinished` | Через сколько секунд, после успешного выполнения, удалить всю информацию о `Job`                       | example:`600`                         |             |
| `suspend`                 | Приостановить запуск Pod                                                                               | example:`false` (по умолчанию)        |             |
| `selector`                | Label selector, для поиска Pod'ов, запущенных в этом `Job`                                             | link:`LabelSelector`                  |             |
| `manualSelector`          | Самостоятельно задать label selector. По умолчанию `Job` сам настраивает label selector для своих Pod. | example:`false` (по умолчанию)        |             |

### Пример

```yaml
kind: Job
apiVersion: batch/v1
metadata:
  name: job
spec:
  template:
    spec:
      containers:
        - name: busybox-cont
          image: busybox
          command: [ "/bin/sh", "-c", "exit 0" ]
```

```yaml
kind: Job
apiVersion: batch/v1
metadata:
  name: report-job
spec:
  parallelism: 1
  completions: 1
  completionMode: NonIndexed
  backoffLimit: 6
  activeDeadlineSeconds: 600
  ttlSecondsAfterFinished: 0
  suspend: false
  manualSelector: false
  template:
    spec:
      containers:
        - name: report-cont
          image: my-report:1
          command: [ "java", "-jar", "report.jar" ]
```