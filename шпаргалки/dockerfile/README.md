# Заголовок

Dockerfile

# Содержание

## Базовый образ

```dockerfile
FROM <image> [AS <name>]
```

(FROM выделить)

### Пример

```dockerfile
FROM openjdk:17-slim
```

```dockerfile
FROM maven:3-openjdk-17 AS builder
```

## Команда, запускающая контейнер

```dockerfile
ENTRYPOINT <command>
```

```dockerfile
CMD <command>
```

(ENTRYPOINT и CMD выделить)

### Пример

```dockerfile
FROM alpine
ENTRYPOINT ["ping"]
CMD ["example.com"]
```

```bash
docker image build --tag ping:1 .
```

```bash
docker container run ping:1
```

```bash
docker container run ping:1 google.com
```

## Выполнить команду при сборке образа

```dockerfile
RUN <command>
```

(RUN выделить)

### Пример

```dockerfile
RUN apt update && \
    apt install -y openjdk-17-jre
```

## Добавить файл в образ

```dockerfile
COPY [--chown=<user>:<group>] [--chmod=<perms>] <src>... <dest>
```

```dockerfile
ADD [--chown=<user>:<group>] [--chmod=<perms>] <src>... <dest>
```

(COPY и ADD выделить)

### Пример

```dockerfile
COPY app.jar /dir/
COPY --chown=user:group --chmod=655 files* /dir/
ADD app.jar /dir/
ADD http://example.com/file.txt /dir/file.txt
```

## Задать переменную окружения в образе

```dockerfile
ENV <key>=<value> ...
```

(ENV выделить)

### Пример

```dockerfile
ENV AUTHOR="Anton Lenok"
ENV COURSE=Kubernetes
```

```dockerfile
ENV AUTHOR="Anton Lenok" COURSE=Kubernetes
```

## Проинформировать об использовании порта

```dockerfile
EXPOSE <port>[/<protocol>]
```

(EXPOSE выделить)

### Пример

```dockerfile
EXPOSE 8080
EXPOSE 8080/tcp
```

## Задать пользователя

```dockerfile
USER <user>[:<group>]
```

(USER выделить)

### Пример

```dockerfile
USER user
USER user:group
USER 1000
USER 1000:1000
```

## Задать рабочий каталог

```dockerfile
WORKDIR <path>
```

(WORKDIR выделить)

### Пример

```dockerfile
WORKDIR /app
```
