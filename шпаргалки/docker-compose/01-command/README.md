# Заголовок

Команды docker-compose

# Содержание

## Помощь

```shell
docker-compose SECTION --help
```

### Пример

```shell
docker-compose --help
```

```shell
docker-compose up --help
```

## Запустить контейнеры

```shell
docker-compose up
```

## Запустить контейнеры в фоновом режиме

```shell
docker-compose up --detach
```

## Запустить из произвольного места

```shell
docker-compose --file /dir/my-compose.yaml up
```

## Остановить и очистить контейнеры

```shell
docker-compose down
```

## Показать список запущенных проектов

```shell
docker-compose ls
```

## Показать список запущенных контейнеров

```shell
docker-compose ps
```

## Показать работающие процессы

```shell
docker-compose top
```

## Показать логи

```shell
docker-compose logs
```