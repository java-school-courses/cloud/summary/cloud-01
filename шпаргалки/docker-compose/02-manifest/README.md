# Заголовок

Docker-compose

# Содержание

## Docker-compose

| Название   | Описание                                                           | Ссылка / пример     | Обязательно |
|------------|--------------------------------------------------------------------|---------------------|-------------|
| `version`  | Версия спецификации docker-compose. По умолчанию последняя версия. | example:`3.9`       |             |
| `services` | Описание запускаемых контейнеров                                   | link:`Services`     | Да          |
| `volumes`  | Настройки постоянного хранилища                                    | example:`my-volume` |             |
| `networks` | Настройки сети                                                     | example:`my-net`    |             |
| `configs`  | Хранение открытых конфигураций приложения                          | link:`Configs`      |             |
| `secrets`  | Хранение секретных конфигураций приложения                         | link:`Configs`      |             |

## Services

| Название      | Описание                                                       | Ссылка / пример                | Массив | Обязательно |
|---------------|----------------------------------------------------------------|--------------------------------|--------|-------------|
| `image`       | Базовый образ                                                  | example:`my-web-server:latest` |        | Да          |
| `ports`       | Связывание портов                                              | example:`8080:8080`            | Да     |             |
| `environment` | Переменные окружения                                           | example:`HOST: example.com`    |        |             |
| `volumes`     | Монтирование постоянного хранилища                             | example:`./host_dir:/cont_dir` | Да     |             |
| `networks`    | Подключение сети                                               | example:`my-net`               | Да     |             |
| `restart`     | Политика перезапуска контейнера                                | example:`no` (по умолчанию)    |        |             |
|               |                                                                | example:`always`               |        |             |
|               |                                                                | example:`on-failure`           |        |             |
|               |                                                                | example:`unless-stopped`       |        |             |
| `configs`     | Подключение открытых конфигураций приложения                   | link:`MountConfigs`            | Да     |             |
| `secrets`     | Подключение секретных конфигураций приложения                  | link:`MountConfigs`            | Да     |             |
| `user`        | Пользователь под которым выполнять команды. По умолчанию root. | example:`1000:1000`            |        |             |
| `depends_on`  | Перед запуском дождаться старта другого контейнера             | example:`other-cont`           | Да     |             |
| `deploy`      | Настройки запуска                                              | link:`Deploy`                  |        |             |
| `entrypoint`  | Переопределить `ENTRYPOINT`                                    | example:`["entrypoint.sh"]`    |        |             |
| `command`     | Переопределить `CMD`                                           | example:`["cmd.sh"]`           |        |             |

## Deploy

| Название    | Описание               | Ссылка / пример             | Обязательно |
|-------------|------------------------|-----------------------------|-------------|
| `replicas`  | Количество контейнеров | example:`1`                 |             |
| `resources` | Запрашиваемые ресурсы  | link:`ResourceRequirements` |             |

## ResourceRequirements

| Название       | Описание                      | Ссылка / пример      | Обязательно |
|----------------|-------------------------------|----------------------|-------------|
| `limits`       | Максимально возможные ресурсы | link:`Resources`     |             |
| `reservations` | Запросить ресурсы             | link:`Resources`     |             |

## Resources

| Название | Описание           | Ссылка / пример  | Обязательно |
|----------|--------------------|------------------|-------------|
| `memory` | Оперативная память | example:`1gb`    |             |
|          |                    | example:`300m`   |             |
|          |                    | example:`2048k`  |             |
|          |                    | example:`1024kb` |             |
|          |                    | example:`2b`     |             |
| `cpus`   | Такты процессора   | example:`0.5`    |             |

## Configs

| Название   | Описание                                                         | Ссылка / пример          | Обязательно |
|------------|------------------------------------------------------------------|--------------------------|-------------|
| `file`     | Путь к файлу на компьютере                                       | example:`./my-conf.yaml` |             |
| `external` | Экспортировать конфигурацию, созданную с помощью команды docker? | example:`true`           |             |

## MountConfigs

| Название | Описание                                          | Ссылка / пример               | Обязательно |
|----------|---------------------------------------------------|-------------------------------|-------------|
| `source` | Источник конфигураций                             | example:`my-conf`             |             |
| `target` | Путь к файлу внутри контейнера                    | example:`/app/conf`           |             |
| `uid`    | User ID владельца конфигурационного файла         | example:`1000`                |             |
| `gid`    | Group ID владельца конфигурационного файла        | example:`1000`                |             |
| `mode`   | Unix права доступа к файлу в восьмеричной системе | example:`0444` (по умолчанию) |             |

## Пример

```yaml
services:
  busybox-cont:
    image: busybox
```

```yaml
services:
  web-server:
    image: my-web-server
    ports: [ "8080" ]
    deploy:
      replicas: 2
      resources:
        limits:
          memory: 50M
          cpus: '1'
        reservations:
          memory: 20M
          cpus: '0.5'
```

```yaml
version: '3.9'
services:
  db:
    image: postgres:15
    environment:
      POSTGRES_PASSWORD: my_pass
    volumes:
      - my-volume:/var/lib/postgresql/data
    configs:
      - source: init-db
        target: /docker-entrypoint-initdb.d/
    networks: [ my-net ]
    restart: always

  web-server:
    image: my-web-server:latest
    ports: [ "8080:8080" ]
    networks: [ my-net ]
    volumes: [ ./config/:/config ]
    restart: on-failure
    depends_on: [ db ]
    command: [ "java", "-jar", "app.jar" ]

volumes:
  my-volume:

networks:
  my-net:

configs:
  init-db:
    file: ./init-db.sql

secrets:
  my-secret:
    external: true
```