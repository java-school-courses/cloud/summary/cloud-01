Множество типов объектов, которые помогают управлять Pod'ами.

### Типы

|        Название         | Описание                                                                            |
|:-----------------------:|:------------------------------------------------------------------------------------|
|      `Deployment`       | Запуск Pod в нескольких экземплярах (рекомендуемый способ)                          |
|   `DeploymentConfig`    | Запуск Pod в нескольких экземплярах (доступен только в OpenShift)                   |
|      `ReplicaSet`       | Запуск Pod в нескольких экземплярах (используется "под капотом" в Deployment)       |
| `ReplicationController` | Запуск Pod в нескольких экземплярах (используется "под капотом" в DeploymentConfig) |
|      `StatefulSet`      | Сохранение состояния Pod'а после перезагрузки                                       |
|          `Job`          | Выполнение одноразовой задачи                                                       |
|        `CronJob`        | Выполнение задачи по расписанию                                                     |
|       `DemonSet`        | Закрепить Pod на конкретном Node/Узле (сервере)                                     |

#### Запуск Pod в нескольких экземплярах

|        Название         |    Использует внутри    | Представлен в Kubernetes | Представлен в OpenShift |
|:-----------------------:|:-----------------------:|:------------------------:|:-----------------------:|
|      `Deployment`       |      `ReplicaSet`       |            Да            |           Да            |
|   `DeploymentConfig`    | `ReplicationController` |           Нет            |           Да            |
|      `ReplicaSet`       |          `Pod`          |            Да            |           Да            |
| `ReplicationController` |          `Pod`          |            Да            |           Да            |

### Примеры

#### Deployment

```yaml
kind: Deployment
apiVersion: apps/v1
metadata:
  name: web-server
spec:
  replicas: 2
  selector:
    matchLabels:
      app: web-server-selector
  template:
    metadata:
      labels:
        app: web-server-selector
    spec:
      containers:
        - name: web-server-cont
          image: my-web-server:1
          ports:
            - name: http
              containerPort: 8080
```

#### StatefulSet

```yaml
kind: StatefulSet
apiVersion: apps/v1
metadata:
  name: postgres
spec:
  serviceName: postgres
  replicas: 1
  selector:
    matchLabels:
      app: postgres-selector
  template:
    metadata:
      labels:
        app: postgres-selector
    spec:
      containers:
        - name: postgres-cont
          image: bitnami/postgresql:15
          ports:
            - name: postgres
              containerPort: 5432
          volumeMounts:
            - name: database-storage
              mountPath: /var/lib/postgresql/data
  volumeClaimTemplates:
    - metadata:
        name: database-storage
      spec:
        accessModes:
          - ReadWriteOnce
        storageClassName: my-storage-class
        resources:
          requests:
            storage: 1Gi
```

#### Job

```yaml
kind: Job
apiVersion: batch/v1
metadata:
  name: show-env-job
spec:
  parallelism: 2
  template:
    spec:
      restartPolicy: Never
      containers:
        - name: test-container
          image: busybox
          command: [ "/bin/sh", "-c", "env" ]
```

#### CronJob

```yaml
kind: CronJob
apiVersion: batch/v1
metadata:
  name: show-date-cron-job
spec:
  schedule: "* * * * *"
  jobTemplate:
    spec:
      parallelism: 2
      template:
        spec:
          restartPolicy: Never
          containers:
            - name: test-container
              image: busybox
              args:
                - /bin/sh
                - '-c'
                - date; echo Hello from the Kubernetes cluster
```

#### DeploymentConfig

DeploymentConfig добавляет в Deployment некоторые функции.

Но он может использоваться только в OpenShift.

```yaml
kind: DeploymentConfig
apiVersion: apps.openshift.io/v1
metadata:
  name: web-server
spec:
  replicas: 2
  selector:
    name: web-server-selector
  template:
    metadata:
      labels:
        name: web-server-selector
    spec:
      containers:
        - name: web-server-cont
          image: my-web-server:1
          ports:
            - name: http
              containerPort: 8080
  triggers:
    - type: ConfigChange
```

#### DaemonSet

```yaml
kind: DaemonSet
apiVersion: apps/v1
metadata:
  name: web-server
spec:
  selector:
    matchLabels:
      app: web-server-selector
  template:
    metadata:
      labels:
        app: web-server-selector
    spec:
      containers:
        - name: web-server-cont
          image: my-web-server:1
          ports:
            - containerPort: 8080
```

#### ReplicaSet

```yaml
kind: ReplicaSet
apiVersion: apps/v1
metadata:
  name: web-server
spec:
  selector:
    matchLabels:
      app: web-server-selector
  template:
    metadata:
      labels:
        app: web-server-selector
    spec:
      containers:
        - name: web-server-cont
          image: my-web-server:1
          ports:
            - containerPort: 8080
```

#### ReplicationController

```yaml
kind: ReplicationController
apiVersion: v1
metadata:
  name: web-server
spec:
  selector:
    app: web-server-selector
  template:
    metadata:
      labels:
        app: web-server-selector
    spec:
      containers:
        - name: web-server-cont
          image: my-web-server:1
          ports:
            - containerPort: 8080
```