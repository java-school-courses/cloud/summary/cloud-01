Минимальная единица работы

Запуск приложения в контейнерах

Запускает несколько контейнеров

Совместный доступ к ресурсам для контейнеров внутри:

- Файлы
- Сеть
- и т.д.

### Секции Pod

| Секция            | Описание                              |
|-------------------|---------------------------------------|
| `containers`      | Обычный контейнер                     |
| `initContainers`  | Контейнер для начальной настройки Pod |
| `volumes`         | Настройка постоянного хранилища       |
| `securityContext` | Настройки безопасности и прав доступа | 
| `restartPolicy`   | Политика перезапуска контейнера       | 

### Секции Pod containers

<table border="1"
       style="border-collapse: collapse; width: 100%; border-style: solid; border-color: rgba(0, 0, 0, 0.1); height: 132.53125px;">
    <tbody>
    <tr>
        <td><h4>Секция</h4></td>
        <td><h4>Подсекция</h4></td>
        <td><h4>Описание</h4></td>
    </tr>
    <tr>
        <td><code>name</code></td>
        <td style="border-right-color: rgba(0, 0, 0, 0);"></td>
        <td>Имя Pod'а</td>
    </tr>
    <tr>
        <td><code>image</code></td>
        <td style="border-right-color: rgba(0, 0, 0, 0);"></td>
        <td>Базовый образ</td>
    </tr>
    <tr>
        <td rowspan="5"><code>ports</code></td>
    </tr>
    <tr>
        <td style="border-right-color: rgba(0, 0, 0, 0);"></td>
        <td>Порты контейнера</td>
    </tr>
    <tr>
        <td><code>name</code></td>
        <td>Наименованное работы с портом</td>
    </tr>
    <tr>
        <td><code>containerPort</code></td>
        <td>Номер порта внутри контейнера</td>
    </tr>
    <tr>
        <td><code>protocol</code></td>
        <td>Протокол</td>
    </tr>
    <tr>
        <td rowspan="5"><code>env</code></td>
    </tr>
    <tr>
        <td style="border-right-color: rgba(0, 0, 0, 0);"></td>
        <td>Точечная передача переменных окружения</td>
    </tr>
    <tr>
        <td><code>name</code></td>
        <td>Имя переменной окружения внутри</td>
    </tr>
    <tr>
        <td><code>value</code></td>
        <td>Значение переменной окружения</td>
    </tr>
    <tr>
        <td><code>valueFrom</code></td>
        <td>Подключение конкретной переменной окружения из ConfigMap или Secret</td>
    </tr>
    <tr>
        <td rowspan="4"><code>envFrom</code></td>
    </tr>
    <tr>
        <td style="border-right-color: rgba(0, 0, 0, 0);"></td>
        <td>Массовая передача переменных окружения из ConfigMap или Secret</td>
    </tr>
    <tr>
        <td><code>configMapRef</code></td>
        <td>Подключение всех переменных из ConfigMap</td>
    </tr>
    <tr>
        <td><code>secretRef</code></td>
        <td>Подключение всех переменных из Secret</td>
    </tr>
    <tr>
        <td><code>volumeMounts</code></td>
        <td style="border-right-color: rgba(0, 0, 0, 0);"></td>
        <td>Монтирование хранилища в контейнер (связана с <code>volumes</code> в Pod'е)</td>
    </tr>
    <tr>
        <td><code>restartPolicy</code></td>
        <td style="border-right-color: rgba(0, 0, 0, 0);"></td>
        <td>Политика перезапуска контейнера</td>
    </tr>
    <tr>
        <td><code>livenessProbe</code></td>
        <td style="border-right-color: rgba(0, 0, 0, 0);"></td>
        <td>Нужно ли перезапустить контейнер?</td>
    </tr>
    <tr>
        <td><code>readinessProbe</code></td>
        <td style="border-right-color: rgba(0, 0, 0, 0);"></td>
        <td>Можно ли подавать трафик на контейнер?</td>
    </tr>
    <tr>
        <td><code>startupProbe</code></td>
        <td style="border-right-color: rgba(0, 0, 0, 0);"></td>
        <td>Контейнер уже запущен?</td>
    </tr>
    <tr>
        <td><code>command</code></td>
        <td style="border-right-color: rgba(0, 0, 0, 0);"></td>
        <td>Команда запуска контейнера</td>
    </tr>
    <tr>
        <td><code>args</code></td>
        <td style="border-right-color: rgba(0, 0, 0, 0);"></td>
        <td>Аргументы запуска контейнера</td>
    </tr>
    <tr>
        <td><code>workingDir</code></td>
        <td style="border-right-color: rgba(0, 0, 0, 0);"></td>
        <td>Рабочая директория запуска</td>
    </tr>
    </tbody>
</table>

### Примеры

#### Минимальный Manifest

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: busybox
spec:
  containers:
    - name: busybox-cont
      image: busybox
```

#### Web приложение

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: web-server
  labels:
    app: web-server
spec:
  containers:
    - name: web-server-cont
      image: my-web-server:1
      ports:
        - name: http
          containerPort: 8080
```

#### Настройка команд запуска

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: ping
  labels:
    app: ping
spec:
  restartPolicy: Never
  containers:
    - name: ping-cont
      image: busybox
      command: [ "/bin/sh", "-c" ]
      args:
        - "ping -c 3 example.com"
```