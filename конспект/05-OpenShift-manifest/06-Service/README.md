Сущность, которая предоставляет сетевой доступ к приложению в Pod'е.

### Deployment

```yaml
kind: Deployment
apiVersion: apps/v1
metadata:
  name: addressbook-deployment
  labels:
    app: addressbook-label-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: addressbook-selector
  template:
    metadata:
      labels:
        app: addressbook-selector
    spec:
      containers:
        - name: addressbook-cont
          image: registry/addressbook:1
          ports:
            - name: http
              containerPort: 8080
```

### Service

```yaml
kind: Service
apiVersion: v1
metadata:
  name: addressbook-service
spec:
  selector:
    app: addressbook-selector
  ports:
    - name: addressbook-api
      targetPort: http
      port: 9090
      protocol: TCP
```