Разберём как хранить и подключать конфигурации к приложению, запущенному в Kubernetes или OpenShift.

### Характеристики конфигураций

- Хранилище Key/Value;
- Key и Value всегда текст;
- Объект до 1 Мб.

#### Разрешенные символы

- Латинские буквы
- Цифры
- `.`
- `_`
- `-`

### Способы работы с конфигурациями

#### ConfigMap

Открытая информация.

###### Пример

```yaml
kind: ConfigMap
apiVersion: v1
metadata:
  name: db-config
data:
  POSTGRESQL_USERNAME: addressbook
  POSTGRESQL_DATABASE: addressbook_db
```

#### Secret

"Чувствительная" информация.

###### Пример

```yaml
kind: Secret
apiVersion: v1
metadata:
  name: db-secret
stringData:
  POSTGRESQL_PASSWORD: my_pass
```

#### Pod Manifest

Переменные окружения Pod'а.

###### Пример

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: show-config-env-v1
spec:
  restartPolicy: Never
  containers:
    - name: test-container
      image: busybox
      command: [ "/bin/sh", "-c", "env" ]
      env:
        - name: VAR_PASSWORD
          value: my_pass
        - name: VAR_USERNAME
          value: addressbook
        - name: VAR_DATABASE
          value: addressbook_db
```

### Подключение

#### Как переменные окружения

##### Всё содержание объекта

ConfigMap

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: show-config-env-v3
spec:
  restartPolicy: Never
  containers:
    - name: test-container
      image: busybox
      command: [ "/bin/sh", "-c", "env | grep POSTGRESQL" ]
      envFrom:
        - configMapRef:
            name: db-config
```

Secret

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: show-config-env-v3
spec:
  restartPolicy: Never
  containers:
    - name: test-container
      image: busybox
      command: [ "/bin/sh", "-c", "env | grep POSTGRESQL" ]
      envFrom:
        - secretRef:
            name: db-secret
```

##### Конкретный key

ConfigMap

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: show-config-env-v2
spec:
  restartPolicy: Never
  containers:
    - name: test-container
      image: registry.k8s.io/busybox
      command: [ "/bin/sh", "-c", "env | grep VAR_" ]
      env:
        - name: VAR_USERNAME
          valueFrom:
            configMapKeyRef:
              name: db-config
              key: POSTGRESQL_USERNAME
```

Secret

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: show-config-env-v2
spec:
  restartPolicy: Never
  containers:
    - name: test-container
      image: registry.k8s.io/busybox
      command: [ "/bin/sh", "-c", "env | grep VAR_" ]
      env:
        - name: VAR_PASSWORD
          valueFrom:
            secretKeyRef:
              name: db-secret
              key: POSTGRESQL_PASSWORD
```

#### Как файл

---START---
ConfigMap
---FINISH---

```yaml
kind: ConfigMap
apiVersion: v1
metadata:
  name: file-config-v1
data:
  application.yaml: |
    spring:
      r2dbc:
        url: r2dbc:postgres://db:5432/postgres
        username: postgres
        password: mysecretpassword
      jpa:
        database-platform: org.hibernate.dialect.PostgreSQL95Dialect
```

---START---
Pod + ConfigMap
---FINISH---

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: show-config-file-v1
spec:
  restartPolicy: Never
  containers:
    - name: test-container
      image: busybox
      command: [ "/bin/sh", "-c", "cat /app/config/application.yaml" ]
      volumeMounts:
        - name: file-config-volume
          mountPath: /app/config/
  volumes:
    - name: file-config-volume
      configMap:
        name: file-config-v1
```

---START---
Secret

```yaml
kind: Secret
apiVersion: v1
metadata:
  name: file-secret-v1
stringData:
  application.yaml: |
    spring:
      r2dbc:
        url: r2dbc:postgres://db:5432/postgres
        username: postgres
        password: mysecretpassword
      jpa:
        database-platform: org.hibernate.dialect.PostgreSQL95Dialect
```

Pod + Secret

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: show-secret-file-v1
spec:
  restartPolicy: Never
  containers:
    - name: test-container
      image: busybox
      command: [ "/bin/sh", "-c", "cat /app/config/application.yaml" ]
      volumeMounts:
        - name: file-secret-volume
          mountPath: /app/config/
  volumes:
    - name: file-secret-volume
      secret:
        secretName: file-secret-v1
```

---FINISH---