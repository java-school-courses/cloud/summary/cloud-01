### Список рекомендованных курсов

* [Основы Kubernetes](https://hr.sberbank.ru/platform/catalog/b137c831-cd1b-49c0-bc6c-db0f0e51b59a)
* [Spring Core](https://hr.sberbank.ru/platform/catalog/44bfd096-7909-4331-a1dd-86566ffb3fc9)
* [Spring Web](https://hr.sberbank.ru/platform/catalog/44bfd096-7909-4331-a1dd-86566ffb3fc9)
* [Spring Data](https://hr.sberbank.ru/platform/catalog/44bfd096-7909-4331-a1dd-86566ffb3fc9)
* [Введение в Service mesh на основе Istio](https://hr.sberbank.ru/platform/catalog/f41f9d7d-7640-405a-bfee-9c3f05adc32f)
* [Интеграция микросервисов в Сбере](https://hr.sberbank.ru/platform/catalog/e0167ac3-664b-4aa7-b989-bb7407a5a18b)
