### Список рекомендованных ресурсов

#### Kubernetes и OpenShift

* [Шпаргалка по kubectl](https://kubernetes.io/ru/docs/reference/kubectl/cheatsheet/)
* [Документация по всем элементам Manifest](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/)

#### Docker

* [Документация команд Docker](https://docs.docker.com/engine/reference/commandline/cli/)
* [Документация Docker-compose](https://docs.docker.com/compose/compose-file/05-services/)
* [Документация Dockerfile](https://docs.docker.com/engine/reference/builder/)
* [Документация многошаговая сборка Docker образов](https://docs.docker.com/build/building/multi-stage/)

#### Работа с Docker образами в Сбере

* [Документация SberOSC](https://confluence.sberbank.ru/display/IH/Docker)
* [Docker registry в Сбере](https://registry-ci.delta.sbrf.ru)
* [Официальный каталог Docker образов Red Hat](https://catalog.redhat.com/software/containers/search)

#### Jib

* [Документация Jib в Сбере](https://mapp.sberbank.ru/app/mapp/878750022)
* [Официальная документация Jib Maven plugin](https://github.com/GoogleContainerTools/jib/tree/master/jib-maven-plugin)
* [Официальная документация Jib Gradle plugin](https://github.com/GoogleContainerTools/jib/tree/master/jib-gradle-plugin)

#### Разработка приложений

* [Генератор Spring приложения](https://start.spring.io)
* [Документация Docker образа `bitnami/postgresql`](https://hub.docker.com/r/bitnami/postgresql)
