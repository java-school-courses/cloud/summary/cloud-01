Docker registry - это хранилище Docker образов.

#### Авторизация в Docker Hub

```shell
docker login
```

#### Push образа в Docker Hub

```shell
docker image push LOGIN/IMAGE_NAME:VERSION
```

### Открытые Docker registry

Самые популярные открытые Docker registry:

<table border="1"
       style="border-collapse: collapse; width: 100%; border-style: solid; border-color: rgba(0, 0, 0, 0.1); height: 132.53125px;">
    <tbody>
    <tr>
        <td style="text-align: center;"><h4>Владелец</h4></td>
        <td style="text-align: center;"><h4>Адрес</h4></td>
    </tr>
    <tr>
        <td style="text-align: center;">Docker Hub</td>
        <td style="text-align: center;"><code>hub.docker.com</code></td>
    </tr>
    <tr>
        <td style="text-align: center;">GitHub</td>
        <td style="text-align: center;"><code>ghcr.io</code></td>
    </tr>
    <tr>
        <td style="text-align: center;" rowspan="3">Red Hat</td>
        <td style="text-align: center;"><code>registry.connect.redhat.com</code></td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>registry.redhat.io</code></td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>quay.io</code></td>
    </tr>
    <tr>
        <td style="text-align: center;" rowspan="3">Google</td>
        <td style="text-align: center;"><code>gcr.io</code></td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>k8s.gcr.io</code></td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>registry.k8s.io</code></td>
    </tr>
    <tr>
        <td style="text-align: center;">Microsoft</td>
        <td style="text-align: center;"><code>mcr.microsoft.com</code></td>
    </tr>
    <tr>
        <td style="text-align: center;">Nvidia</td>
        <td style="text-align: center;"><code>catalog.ngc.nvidia.com</code></td>
    </tr>
    </tbody>
</table>

### Docker registry в Сбере

registry-ci.delta.sbrf.ru

#### Авторизация в Docker registry Сбера

```shell
docker login registry-ci.delta.sbrf.ru
```

После этого вводите логин и пароль от учетной записи в Сбере.

Для отправки Docker образа в `registry-ci.delta.sbrf.ru` рекомендуется использовать техническую учетную запись.

#### Выход из учетной записи

```bash
docker logout registry-ci.delta.sbrf.ru
```

#### Push образа в Docker registry Сбера

```shell
docker image push docker-dev.registry-ci.delta.sbrf.ru/КЭ_АС/КЭ_ИТ/IMAGE_NAME:VERSION
```

#### Главные разделы Docker registry в Сбере

- Базовые образы
    - base
    - redhat
- Хранилище образов с приложениями
    - docker-dev – тестовые стенды
    - docker-release – промышленные стенды

