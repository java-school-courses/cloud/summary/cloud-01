### Зеркало

Хранилище Docker образов внутри Сбера.

Которое забирает проверенные образы из интернета, например, из Docker Hub.

### Зеркало Red Hat

Зеркалирование хранилища Red Hat.

Отсюда можно забрать образ проверенный Red Hat.

#### Адрес в Сбере

`registry-ci.delta.sbrf.ru/redhat`

#### Официальный каталог Docker образов Red Hat

https://catalog.redhat.com/software/containers/search

#### Преобразование официальных ссылок в сберовские

##### Официальный адрес

registry.access.redhat.com/**ubi9/openjdk-17-runtime**

##### Сберовский адрес

registry-ci.delta.sbrf.ru/redhat/**ubi9/openjdk-17-runtime**

#### Red Hat Universal Base Image (UBI)

Универсальный базовый образ на основе ОС Red Hat.

| Название | Объяснение |
|:---:|---|
|`ubi9/openjdk-17`|Образ с полноценным JDK|
|`ubi9/openjdk-17-runtime`|Образ с JRE|
|`ubi9/ubi-minimal`|Минимальный набор утилит, для запуска приложений, откомпилированных в GraalVM|

### SberOSC

Основное зеркало Docker образов в Сбере.

#### Документация

https://confluence.sberbank.ru/display/IH/Docker

#### Алгоритм получения образа

* Добавить адрес `sberosc.sigma.sbrf.ru` в доверенные в настройках приложения docker

```json
{
  "insecure-registries": [
    "stage.sberosc.sigma.sbrf.ru"
  ]
}
```

* Авторизоваться на сайте `sberosc.sigma.sbrf.ru`
* Получить токен авторизации на сайте `sberosc.sigma.sbrf.ru`
* В приложении docker авторизовать в `sberosc.sigma.sbrf.ru`

```shell
docker login --username token stage.sberosc.sigma.sbrf.ru
```

* Загрузить в приложении docker базовый образ

```shell
docker image pull sberosc.sigma.sbrf.ru/docker.io/alpine:3.15
```

#### Список названий областей, доступные в SberOSC

<table border="1"
       style="border-collapse: collapse; width: 100%; border-style: solid; border-color: rgba(0, 0, 0, 0.1); height: 132.53125px;">
    <tbody>
    <tr>
        <td style="text-align: center;"><h4>Владелец</h4></td>
        <td style="text-align: center;"><h4>Адрес</h4></td>
        <td style="text-align: center;"><h4>Название области в SberOSC</h4></td>
        <td><h4>Пример ссылки</h4></td>
    </tr>
    <tr>
        <td style="text-align: center;">Docker Hub</td>
        <td style="text-align: center;"><code>hub.docker.com</code></td>
        <td style="text-align: center;"><code>docker.io</code></td>
        <td><code>sberosc.sigma.sbrf.ru/docker.io/alpine:3.15</code></td>
    </tr>
    <tr>
        <td style="text-align: center;">GitHub</td>
        <td style="text-align: center;"><code>ghcr.io</code></td>
        <td style="text-align: center;"><code>ghcr.io</code></td>
        <td><code>sberosc.sigma.sbrf.ru/ghcr.io/dexidp/dex:v2.35.3-distroless</code></td>
    </tr>
    <tr>
        <td style="text-align: center;" rowspan="3">Red Hat</td>
        <td style="text-align: center;"><code>registry.connect.redhat.com</code></td>
        <td style="text-align: center;"><code>connect.redhat.com</code></td>
        <td><code>sberosc.sigma.sbrf.ru/connect.redhat.com/influxdata/telegraf-1x:1.4.3</code></td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>registry.redhat.io</code></td>
        <td style="text-align: center;"><code>redhat.io</code></td>
        <td><code>sberosc.sigma.sbrf.ru/redhat.io/amq7/amq-streams-rhel8-operator:2.3.0-6.1675788338</code></td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>quay.io</code></td>
        <td style="text-align: center;"><code>quay.io</code></td>
        <td><code>sberosc.sigma.sbrf.ru/quay.io/jetstack/cert-manager-controller:v1.11.0</code></td>
    </tr>
    <tr>
        <td style="text-align: center;" rowspan="3">Google</td>
        <td style="text-align: center;"><code>gcr.io</code></td>
        <td style="text-align: center;"><code>gcr.io</code></td>
        <td><code>sberosc.sigma.sbrf.ru/gcr.io/google-containers/busybox@sha256:545e6a6310a27636260920bc07b994a299b6708a1b26910cfefd335fdfb60d2b</code>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>k8s.gcr.io</code></td>
        <td style="text-align: center;"><code>k8s.gcr.io</code></td>
        <td><code>sberosc.sigma.sbrf.ru/k8s.gcr.io/kube-apiserver:v1.25.3</code></td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>registry.k8s.io</code></td>
        <td style="text-align: center;"><code>registry.k8s.io</code></td>
        <td><code>sberosc.sigma.sbrf.ru/registry.k8s.io/kube-apiserver:v1.26.0</code></td>
    </tr>
    <tr>
        <td style="text-align: center;">Microsoft</td>
        <td style="text-align: center;"><code>mcr.microsoft.com</code></td>
        <td style="text-align: center;"><code>mcr.microsoft.com</code></td>
        <td></td>
    </tr>
    <tr>
        <td style="text-align: center;" rowspan="4">Nvidia</td>
        <td style="text-align: center;"><code>catalog.ngc.nvidia.com</code></td>
        <td style="text-align: center;"><code>nvcr.io/nvidia</code></td>
        <td><code>sberosc.sigma.sbrf.ru/nvcr.io/nvidia/kubevirt-gpu-device-plugin:v1.0.1</code></td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>catalog.ngc.nvidia.com</code></td>
        <td style="text-align: center;"><code>nvcr.io/hpc</code></td>
        <td></td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>catalog.ngc.nvidia.com</code></td>
        <td style="text-align: center;"><code>nvcr.io/nvidia-hpcvis</code></td>
        <td></td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>catalog.ngc.nvidia.com</code></td>
        <td style="text-align: center;"><code>nvcr.io/partner</code></td>
        <td></td>
    </tr>
    </tbody>
</table>