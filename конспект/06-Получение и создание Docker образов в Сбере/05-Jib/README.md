Плагин для Maven или Gradle для сборки Docker образа без приложения docker.

### pom.xml

Чтобы подключить Jib в pom.xml добавьте плагин.

```xml
<build>
    <plugins>
        <plugin>
            <groupId>com.google.cloud.tools</groupId>
            <artifactId>jib-maven-plugin</artifactId>
            <version>${jib-maven-plugin.version}</version>
            <configuration>
                <to>
                    <image>${image.path}</image>
                </to>
            </configuration>
        </plugin>
    </plugins>
</build>
```

### settings.xml

Объявите логин и пароль для авторизации в Docker registry.

Пример для Docker Hub:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<settings xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd"
          xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <servers>
        <server>
            <id>registry.hub.docker.com</id>
            <username>USER</username>
            <password>PASSWORD</password>
        </server>
    </servers>
</settings>
```

### Запуск Jib

Сборка образа и отправка в Docker registry.

```bash
mvn --settings settings.xml compile jib:build
-Dimage=registry.hub.docker.com/LOGIN/IMAGE_NAME:VERSION
```