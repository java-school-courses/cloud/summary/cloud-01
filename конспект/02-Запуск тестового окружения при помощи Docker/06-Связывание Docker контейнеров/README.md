Свяжем контейнер Addressbook с контейнером Postgres.

#### run-docker.sh

Модифицируем скрипт запуска контейнеров.

```bash
#!/usr/bin/env bash

docker container run \
--publish 5432:5432 \
--env POSTGRESQL_DATABASE=addressbook_db \
--env POSTGRESQL_USERNAME=addressbook \
--env POSTGRESQL_PASSWORD=my_pass \
--detach \
--volume ./db/init-db.sql:/docker-entrypoint-initdb.d/init-db.sql \
--name db \
bitnami/postgresql:15

docker container start db

docker container run \
--publish 8080:8080 \
--link db \
--volume ./configuration:/app/config \
addressbook:1
```

#### application.yaml

Addressbook запущенный в контейнере должен иметь следующий `application.yaml`, чтобы правильно обращаться к Postgres.

```yaml
spring:
  r2dbc:
    url: r2dbc:postgres://db:5432/addressbook_db
    username: addressbook
    password: my_pass
  jpa:
    database-platform: org.hibernate.dialect.PostgreSQL95Dialect
```