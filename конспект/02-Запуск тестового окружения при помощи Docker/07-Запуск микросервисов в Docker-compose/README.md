Запускать несколько связанных микросервисов при помощи команд docker - неудобно.

Практичный подход - использовать Docker-compose.

Напишем скрипт запуска контейнеров в Docker-compose.

А так же узнаем самые полезные команды этого приложения.

#### docker-compose.yaml

```yaml
version: '3.9'
services:
  db:
    image: bitnami/postgresql:15
    ports:
      - "5432:5432"
    environment:
      POSTGRESQL_DATABASE: addressbook_db
      POSTGRESQL_USERNAME: addressbook
      POSTGRESQL_PASSWORD: my_pass
    volumes:
      - ./db/:/docker-entrypoint-initdb.d/
      - data-db:/bitnami/postgresql

  addressbook:
    image: addressbook:1
    ports:
      - "8080:8080"
    volumes:
      - ./configuration:/app/config

volumes:
  data-db:
```

#### Запуск системы в docker-compose

```bash
docker-compose up
```

#### Запуск системы в docker-compose в фоновом режиме

```bash
docker-compose up --detach
```

#### Остановка системы в docker-compose

```bash
docker-compose down
```