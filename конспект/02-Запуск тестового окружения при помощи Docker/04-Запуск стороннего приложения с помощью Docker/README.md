Установка Postgres на свой компьютер не тривиальная задача.

Эта задача превращает разработчика в системного администратора.

Docker позволяет решить её в одну команду.

Возьмем образ `bitnami/postgresql`, который адаптирован для Kubernetes и OpenShift.

#### Документация образа bitnami/postgresql

[bitnami/postgresql](https://hub.docker.com/r/bitnami/postgresql)

#### Запуск Postgres

```bash
docker container run --publish 5432:5432 --env POSTGRESQL_PASSWORD=my_pass bitnami/postgresql:15
```

#### application.yaml

Приложение Addressbook запускаем в IntelliJ IDEA.

Тогда файл `application.yaml` нужно модифицировать следующим образом:

```yaml
spring:
  r2dbc:
    url: r2dbc:postgres://localhost:5432/postgres
    username: postgres
    password: my_pass
  jpa:
    database-platform: org.hibernate.dialect.PostgreSQL95Dialect
```

#### Скрипт создания схемы БД

Для правильной работы нужно подключиться вручную к базе данных и выполнить скрипт создания схемы.

```sql
DROP TABLE IF EXISTS ADDRESS_BOOK;
CREATE TABLE ADDRESS_BOOK
(
    ID         BIGSERIAL PRIMARY KEY,
    FIRST_NAME VARCHAR(50) NOT NULL,
    LAST_NAME  VARCHAR(50) NOT NULL,
    PHONE      VARCHAR(20),
    BIRTHDAY   DATE
)
```