1) Настроем параметры подключения к базе данных:

- Имя базы данных;
- Логин;
- Пароль.

2) Запустим Postgres в фоновом режиме.

3) Подключим скрипт, который создаст схему базы данных.

#### run-docker.sh

Для удобства работы вынесем команду запуска Docker контейнера в bash скрипт.

```bash
#!/usr/bin/env bash

docker container run \
--publish 5432:5432 \
--env POSTGRESQL_DATABASE=addressbook_db \
--env POSTGRESQL_USERNAME=addressbook \
--env POSTGRESQL_PASSWORD=my_pass \
--detach \
--volume ./db/init-db.sql:/docker-entrypoint-initdb.d/init-db.sql \
bitnami/postgresql:15
```

#### application.yaml

Модифицируем `application.yaml` согласно новым параметрам подключения.

```yaml
spring:
  r2dbc:
    url: r2dbc:postgres://localhost:5432/addressbook_db
    username: addressbook
    password: my_pass
  jpa:
    database-platform: org.hibernate.dialect.PostgreSQL95Dialect
```

#### init-db.sql

```sql
DROP TABLE IF EXISTS ADDRESS_BOOK;
CREATE TABLE ADDRESS_BOOK
(
    ID         BIGSERIAL PRIMARY KEY,
    FIRST_NAME VARCHAR(50) NOT NULL,
    LAST_NAME  VARCHAR(50) NOT NULL,
    PHONE      VARCHAR(20),
    BIRTHDAY   DATE
)
```