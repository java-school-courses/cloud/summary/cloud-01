#### Dockerfile

Напишем алгоритм сборки Docker образа в файле `Dockerfile`.

```dockerfile
FROM openjdk:17-jdk
WORKDIR /app
RUN mkdir /app/config
COPY target/*.jar /app/app.jar
EXPOSE 8080/tcp
ENTRYPOINT ["java", "-jar", "/app/app.jar"]
```

#### Создание Docker образа

Выполним команду сборки, в каталоге, где находится `Dockerfile`.

```bash
docker image build --tag addressbook:1 .
```

#### Запуск Docker контейнера

```bash
docker container run --publish 8080:8080 addressbook:1
```