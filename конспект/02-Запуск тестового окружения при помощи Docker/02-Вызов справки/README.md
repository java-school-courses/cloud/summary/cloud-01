В приложении docker много команд, но самая важная - вызов справки.

Она поможет вам разобраться как работают остальные команды.

#### Нотация

```bash
docker SECTION --help
```

```bash
docker help SECTION
```

#### Пример

```bash
docker --help
```

```bash
docker image --help
```

```bash
docker help image
```