Рассмотрим команды для работы с Docker образом.

### Посмотреть список образов

```bash
docker image ls
```

|Название|Пример|Объяснение|
|:---:|:---:|---|
| `REPOSITORY` | `alpine` | Хост и имя образа |
| `TAG` | `latest` | Версия/тег образа |
| `IMAGE ID` | `b2aa39c304c2` | Уникальный идентификатор образа |
| `CREATED` | `3 months ago` | Время создания образа |
| `SIZE` | `7.05MB` | Размер образа |

### Собрать образ из Dockerfile

```bash
docker image build [OPTIONS] PATH | URL | -
```

Псевдонимы

```bash
docker build [OPTIONS] PATH | URL | -
```

```bash
docker buildx build [OPTIONS] PATH | URL | -
```

```bash
docker buildx b [OPTIONS] PATH | URL | -
```

Пример

```bash
docker image build --tag my_app:1 .
```

```bash
docker image build --tag my_app:1 https://example.com/dockerfile
```

### Создать псевдоним образа

```bash
docker image tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
```

Псевдоним

```bash
docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
```

Пример

```bash
docker image tag my_app:1 docker.io/my_account/my_app:1
```

```bash
docker image tag my_app docker.io/my_account/my_app registry.k8s.io/my_app
```

### Удалить образ

```bash
docker image rm [OPTIONS] IMAGE [IMAGE...]
```

Псевдонимы

```bash
docker image remove [OPTIONS] IMAGE [IMAGE...]
```

```bash
docker rmi [OPTIONS] IMAGE [IMAGE...]
```

Пример

```bash
docker image rm my_app:1
```

```bash
docker image rm my_app docker.io/my_account/my_app
```

### Удалить все образы, не привязанные к контейнерам

```bash
docker image prune [OPTIONS]
```

Пример

```bash
docker image prune --force
```

### Сохранить образ в файл

```bash
docker image save --output FILE_NAME IMAGE [IMAGE...]
```

Псевдоним

```bash
docker save -o FILE_NAME IMAGE [IMAGE...]
```

Пример

```bash
docker image save --output my_app.tar my_app:1
```

### Загрузить образ из файла

```bash
docker image load --input FILE_NAME
```

Псевдоним

```bash
docker load --input FILE_NAME
```

Пример

```bash
docker image load --input my_app.tar
```

### Загрузить образ из удалённого хранилища

```bash
docker image pull [OPTIONS] NAME[:TAG|@DIGEST]
```

Псевдоним

```bash
docker pull [OPTIONS] NAME[:TAG|@DIGEST]
```

Пример

```bash
docker image pull alpine:3.17
```

```bash
docker image pull registry.k8s.io/busybox
```

### Отправить образ в удалённое хранилище

```bash
docker image push [OPTIONS] NAME[:TAG]
```

Псевдоним

```bash
docker push [OPTIONS] NAME[:TAG]
```

Пример

```bash
docker image push docker.io/my_account/my_app:1
```