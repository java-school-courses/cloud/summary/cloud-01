### Авторизоваться в удалённом хранилище

Нужно авторизоваться в прижении docker, чтобы работать с закрытыми хранилищами Docker образов, например - хранилищем
Сбера `registry-ci.delta.sbrf.ru`.

#### Авторизация

```bash
docker login [OPTIONS] [SERVER] [flags]
```

Пример

```bash
docker login registry-ci.delta.sbrf.ru
Username: TYZ-123
Password: ???????
```

```bash
docker login --username TYZ-123 --password ??????? registry-ci.delta.sbrf.ru
```

#### Выход из учетной записи

```bash
docker logout [SERVER] [flags]
```

Пример

```bash
docker logout registry-ci.delta.sbrf.ru
```