### Работа с контейнерами

Здесь собраны команды, которые могут вам пригодиться для взаимодействия с приложением, запущенным в контейнере.

#### Запуск

```bash
docker container run [OPTIONS] IMAGE
```

Псевдоним

```bash
docker run [OPTIONS] IMAGE
```

Пример

```bash
docker container run busybox
```

#### Посмотреть список контейнеров

Только работающие контейнеры

```bash
docker container ls [OPTIONS]
```

Псевдоним

```bash
docker container list [OPTIONS]
```

```bash
docker container ps [OPTIONS]
```

```bash
docker ps [OPTIONS]
```

Пример

```bash
docker container ls
```

|Название|Пример|Объяснение|
|:---:|:---:|---|
| `CONTAINER ID` | `b1d92061d50c` | Уникальная хеш-сумма контейнера |
| `IMAGE` | `busybox` | Имя базового Docker-образа |
| `COMMAND` | `"sleep 100"` | Стартовая команда в контейнере |
| `CREATED` | `4 seconds ago` | Время, когда был создан контейнер |
| `STATUS` | `Up 3 seconds` | Статус работы контейнера |
| `PORTS` | `0.0.0.0:80->80/tcp` | Опубликованные порты |
| `NAMES` | `my_container` | Имя контейнера |

Все контейнеры (работающие и остановленные)

```bash
docker container ls --all
```

```bash
docker container ls -a
```

#### Остановить контейнер

```bash
docker container stop [OPTIONS] CONTAINER [CONTAINER...]
```

Псевдоним

```bash
docker stop CONTAINER
```

Пример

```bash
docker container stop my_container
```

#### Запуск ранее остановленного контейнера

```bash
docker container start [OPTIONS] CONTAINER [CONTAINER...]
```

Псевдоним

```bash
docker start CONTAINER
```

Пример

```bash
docker container start my_container
```

#### Перезапуск контейнера

```bash
docker container restart [OPTIONS] CONTAINER [CONTAINER...]
```

Псевдоним

```bash
docker restart [OPTIONS] CONTAINER
```

Пример

```bash
docker container restart my_container
```

#### Удалить контейнер

```bash
docker container remove [OPTIONS] CONTAINER [CONTAINER...]
```

Псевдонимы

```bash
docker container rm CONTAINER
```

```bash
docker rm CONTAINER
```

Пример

```bash
docker container rm my_container
```

```bash
docker container rm my_container other_container
```

#### Удалить все остановленные контейнеры

```bash
docker container prune [OPTIONS]
```

Пример

```bash
docker container prune
```

#### Выполнить команду внутри запущенного контейнера

```bash
docker container exec [OPTIONS] CONTAINER COMMAND [ARG...]
```

Пример

```bash
docker container exec my_container ls -l
```

#### Скопировать файлы из контейнера

```bash
docker container cp [OPTIONS] CONTAINER:SRC_PATH DEST_PATH
```

```bash
docker container cp [OPTIONS] SRC_PATH CONTAINER:DEST_PATH
```

Псевдонимы

```bash
docker cp [OPTIONS] CONTAINER:SRC_PATH DEST_PATH
```

```bash
docker cp [OPTIONS] SRC_PATH CONTAINER:DEST_PATH
```

Пример

```bash
docker container cp my_container:/work/out.log out.log
```

```bash
docker container cp my_container:/work/out.log .
```

```bash
docker container cp out.log my_container:/work/
```

```bash
docker container cp ./config my_container:/work/config
```

#### Показать лог из контейнера

```bash
docker container logs [OPTIONS] CONTAINER
```

Псевдоним

```bash
docker logs CONTAINER
```

Пример

```bash
docker container logs my_container
```