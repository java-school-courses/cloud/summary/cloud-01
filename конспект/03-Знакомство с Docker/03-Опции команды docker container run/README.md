Самая популярная команда, которой вы будете пользоваться - `docker container run`, для запуска контейнера.

Рассмотрим параметры этой команды.

### Подключить порт из контейнера на компьютер запуска

```bash
docker container run --publish CONTIANER_PORT:HOST_PORT IMAGE
```

Псевдоним

```bash
docker container run -p CONTIANER_PORT:HOST_PORT IMAGE
```

Пример

```bash
docker container run --publish 8080:8080 busybox
```

### Смонтировать файл или каталог

Подключить файл или каталог из компьютера запуска внутрь контейнера

```bash
docker container run --volume HOST_FILE:CONTAINER_FILE IMAGE
```

Псевдоним

```bash
docker container run -v HOST_FILE:CONTAINER_FILE IMAGE
```

Пример

```bash
docker container run --volume ./config/application.yaml:/app/config/application.yaml busybox
```

```bash
docker container run --volume ./config:/app/config/ busybox
```

### Добавить переменную окружения

```bash
docker container run --env KEY=VALUE IMAGE
```

Псевдоним

```bash
docker container run -e KEY=VALUE IMAGE
```

Пример

```bash
docker container run --env POSTGRES_PASSWORD=my_pass postgres:15
```

### Выполнение в фоновом режиме

```bash
docker container run --detach IMAGE
```

Псевдоним

```bash
docker container run -d IMAGE
```

Пример

```bash
docker container run --detach busybox
```

### Явно задать имя контейнеру

```bash
docker container run --name CONTAINER_NAME IMAGE
```

Пример

```bash
docker container run --name my_container busybox
```

### Автоматическое удаление контейнера после остановки

```bash
docker container run --rm IMAGE
```

Пример

```bash
docker container run --rm busybox
```

### Подключиться внутрь контейнера

```bash
docker container run -it IMAGE
```

```bash
docker container run --interactive --tty IMAGE
```

Пример

```bash
docker container run -it busybox
```