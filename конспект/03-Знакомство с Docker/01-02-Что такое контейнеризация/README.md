Контейнеризация - способ упаковки приложения в контейнер вместе с облегчённой операционной системой, необходимыми
библиотеками и зависимостями.

Такой контейнер можно запустить на любой инфраструктуре и он будет вести себя одним и тем же образом, потому что все
зависимости, включая операционную систему, идентичны.

От классической виртуализации, контейнеризация отличается тем, что контейнер не стремится полностью эмулировать систему
в виртуальной машине. Контейнер использует то же ядро операционной системы, в котором он запущен.

Это позволяет получить преимущества изоляции как в вирутальной машине, но не тратить ресурсы на поддержание нескольких
независимых операционных систем.

### Сравнение виртуальной машины и контейнера

<table border="1" style="border-collapse: collapse; width: 100%; border-style: solid; border-color: rgba(0, 0, 0, 0.1); height: 132.53125px;">
<tbody>
<tr>
<td style="text-align: center;" colspan="2"><h4>Вирутальная машина</h4></td>
</tr><tr>
<td style="text-align: center;">App1</td>
<td style="text-align: center;">App2</td>
</tr>
<tr>
<td style="text-align: center;">Библиотеки</td>
<td style="text-align: center;">Библиотеки</td>
</tr>
<tr>
<td style="text-align: center;">Гостевая операционная система</td>
<td style="text-align: center;">Гостевая операционная система</td>
</tr>
<tr>
<td style="text-align: center;" colspan="2">Гипервизор</td>
</tr>
<tr>
<td style="text-align: center;" colspan="2">Инфраструктура</td>
</tr>
</tbody>
</table>

<table border="1" style="border-collapse: collapse; width: 100%; border-style: solid; border-color: rgba(0, 0, 0, 0.1); height: 132.53125px;">
<tbody>
<tr>
<td style="text-align: center;" colspan="2"><h4>Контейнеры</h4></td>
</tr><tr>
<td style="text-align: center;">App1</td>
<td style="text-align: center;">App2</td>
</tr>
<tr>
<td style="text-align: center;">Библиотеки</td>
<td style="text-align: center;">Библиотеки</td>
</tr>
<tr>
<td style="text-align: center;" colspan="2">Средство контейнеризации</td>
</tr>
<tr>
<td style="text-align: center;" colspan="2">Операционная система</td>
</tr>
</tbody>
</table>

### Технологии ядра Linux в основе контейнеризации

#### Cgroup - контрольная группа

Разделяет ресурсы между процессами:

- Доля тактов ЦП;
- Мегабайты ОЗУ;
- Время обработки ввода/вывода;
- и т.д.

#### Namespace - пространство имён

Создаёт для приложение видимость, что вокруг него ничего нет. Как-будто в операционной системе запущено только это
приложение.