### Многошаговая сборка

Подготовка образа с приложением в несколько этапов, используя несколько базовых образов.

Обычно применяется, чтобы откомпилировать приложение в одном образе, а выполнять в другом.

#### Документация

https://docs.docker.com/build/building/multi-stage/

#### Пример

```dockerfile
FROM maven:3-openjdk-17 AS builder
WORKDIR /build
COPY * .
RUN mvn package

FROM openjdk:17-slim
WORKDIR /app
RUN mkdir /app/config
COPY --from=builder /build/target/*.jar app.jar
EXPOSE 8080/tcp
ENTRYPOINT ["java", "-jar", "/app/app.jar"]
```