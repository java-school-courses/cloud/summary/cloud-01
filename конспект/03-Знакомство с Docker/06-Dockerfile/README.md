Описывает команды, которые выполняются во время создания Docker образа.

### Документация

https://docs.docker.com/engine/reference/builder/

### FROM

Задать базовый образ

```dockerfile
FROM alpine
```

### ENTRYPOINT / CMD

Основная команда, которая запустится внутри контейнера

В начале запускается `ENTRYPOINT`, затем он запускает `CMD`

#### ENTRYPOINT

Приложение запустит сразу ping с адресом-настройкой

```dockerfile
FROM alpine
ENTRYPOINT ["ping", "www.google.com"]
```

#### CMD

Приложение запустит ping с возможностью переопределить адрес-настройку

```dockerfile
FROM alpine
ENTRYPOINT ["ping"]
CMD ["www.google.com"]
```

Если собрать образ командой

```bash
docker image build --tag ping:1 .
```

То потом можно запускать контейнер без параметров

```bash
docker container run ping:1
```

Или с параметрами

```bash
docker container run ping:1 yandex.ru
```

### COPY / ADD

Добавить файлы в образ

При этом ADD - продвинутая версия COPY, умеет:

- Загрузка файлов из интернета
- Распаковывать архивы

Если вам специально не нужны эти функции Лучше использовать COPY

#### Загрузка файлов из интернета

```dockerfile
FROM alpine
WORKDIR /app
ADD https://github.com/dotcloud/docker/archive/master.tar.gz .
ENTRYPOINT ["ls", "-lh"]
```

#### Распаковка архива

```dockerfile
FROM alpine
WORKDIR /app
ADD files.tar.gz .
ENTRYPOINT ["ls", "-lh"]
```

#### .dockerignore

Фильтрует файлы для команд COPY и ADD

Работает так же, как `.gitignore`

```bash
*
!target/*.jar
```

### RUN

Выполнить linux команду во время формирования образа

#### Пример

Сделать образ содержащий интерпретатор Python и Java виртуальную машину

```dockerfile
FROM python

RUN apt-get update && \
    apt-get install -y openjdk-17-jre-headless

CMD [ "java", "--version" ]
```

### ENV

Задать переменную окружения в образе с приложением

```dockerfile
FROM bitnami/postgresql:15
ENV POSTGRESQL_PASSWORD my_pass
```

### EXPOSE

Объявить, что в образе используется порт

Замечание: это не гарантирует, что порт будет открыт

```dockerfile
FROM openjdk:17-jdk
WORKDIR /app
RUN mkdir /app/config
COPY target/*.jar /app/app.jar
EXPOSE 8080/tcp
ENTRYPOINT ["java", "-jar", "/app/app.jar"]
```

### USER

Пользователь, от которого будет запущена `ENTRYPOINT` и `CMD` внутри контейнера

```dockerfile
FROM ubuntu
RUN groupadd -r user && useradd -r -g user user
USER user
CMD ["whoami"]
```

### WORKDIR

Рабочая директория, из которой стартует контейнер

```dockerfile
FROM openjdk:17-jdk
WORKDIR /app
RUN mkdir /app/config
COPY target/*.jar /app/app.jar
EXPOSE 8080/tcp
ENTRYPOINT ["java", "-jar", "app.jar"]
```

