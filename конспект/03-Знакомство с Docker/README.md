# Команды Docker

## Помощь

### Нотация

```shell
docker SECTION --help
```

```shell
docker help SECTION
```

### Пример

```shell
docker --help
```

```shell
docker image --help
```

```shell
docker help image
```

## Работа с контейнерами

### Запуск

```shell
docker container run [OPTIONS] IMAGE
```

Псевдоним

```shell
docker run [OPTIONS] IMAGE
```

Пример

```shell
docker container run busybox
```

### Посмотреть список контейнеров

#### Только работающие контейнеры

```shell
docker container ls [OPTIONS]
```

Псевдоним

```shell
docker container list [OPTIONS]
```

```shell
docker container ps [OPTIONS]
```

```shell
docker ps [OPTIONS]
```

```shell
docker container ls

CONTAINER ID   IMAGE     COMMAND       CREATED         STATUS       PORTS                NAMES
b1d92061d50c   busybox   "sleep 100"   4 seconds ago   Up 3 seconds 0.0.0.0:80->80/tcp   my_container
[----------]   [-----]   [---------]   [-----------]   [----------] [----------------]   [----------]
     |            |           |              |               |               |                 |
     |            |           |              |               |               |                 +--> Имя контейнера
     |            |           |              |               |               +--------------------> Опубликованные порты
     |            |           |              |               +------------------------------------> Статус работы контейнера
     |            |           |              +----------------------------------------------------> Время, когда был создан контейнер
     |            |           +-------------------------------------------------------------------> Стартовая команда в контейнере
     |            +-------------------------------------------------------------------------------> Имя базового Docker-образа
     +--------------------------------------------------------------------------------------------> Уникальная хеш-сумма контейнера
```

#### Все контейнеры (работающие и остановленные)

```shell
docker container ls -a
```

```shell
docker container ls --all
                                          
CONTAINER ID   IMAGE     COMMAND       CREATED         STATUS                     PORTS                NAMES
b1d92061d50c   busybox   "sleep 100"   4 seconds ago   Up 3 seconds               0.0.0.0:80->80/tcp   my_container
75476f58701a   busybox   "sleep 100"   3 minutes ago   Exited (0) 2 minutes ago                        other_container
```

### Остановить контейнер

```shell
docker container stop [OPTIONS] CONTAINER [CONTAINER...]
```

Псевдоним

```shell
docker stop CONTAINER
```

Пример

```shell
docker container stop my_container
```

### Запуск ранее остановленного контейнера

```shell
docker container start [OPTIONS] CONTAINER [CONTAINER...]
```

Псевдоним

```shell
docker start CONTAINER
```

Пример

```shell
docker container start my_container
```

### Перезапуск контейнера

```shell
docker container restart [OPTIONS] CONTAINER [CONTAINER...]
```

Псевдоним

```shell
docker restart [OPTIONS] CONTAINER
```

Пример

```shell
docker container restart my_container
```

### Удалить контейнер

```shell
docker container remove [OPTIONS] CONTAINER [CONTAINER...]
```

Псевдонимы

```shell
docker container rm CONTAINER
```

```shell
docker rm CONTAINER
```

Пример

```shell
docker container rm my_container
```

```shell
docker container rm my_container other_container
```

### Удалить все остановленные контейнеры

```shell
docker container prune [OPTIONS]
```

Пример

```shell
docker container prune
```

### Выполнить команду внутри запущенного контейнера

```shell
docker container exec [OPTIONS] CONTAINER COMMAND [ARG...]
```

Пример

```shell
docker container exec my_container ls -l
```

### Скопировать файлы из контейнера

```shell
docker container cp [OPTIONS] CONTAINER:SRC_PATH DEST_PATH
```

```shell
docker container cp [OPTIONS] SRC_PATH CONTAINER:DEST_PATH
```

Псевдонимы

```shell
docker cp [OPTIONS] CONTAINER:SRC_PATH DEST_PATH
```

```shell
docker cp [OPTIONS] SRC_PATH CONTAINER:DEST_PATH
```

Пример

```shell
docker container cp my_container:/work/out.log out.log
```

```shell
docker container cp my_container:/work/out.log .
```

```shell
docker container cp out.log my_container:/work/
```

```shell
docker container cp ./config my_container:/work/config
```

### Показать лог из контейнера

```shell
docker container logs [OPTIONS] CONTAINER
```

Псевдоним

```shell
docker logs CONTAINER
```

Пример

```shell
docker container logs my_container
```

## Опции запуска контейнера

### Подключить порт из контейнера на компьютер запуска

```shell
docker container run --publish CONTIANER_PORT:HOST_PORT IMAGE
```

Псевдоним

```shell
docker container run -p CONTIANER_PORT:HOST_PORT IMAGE
```

Пример

```shell
docker container run --publish 8080:8080 busybox
```

### Смонтировать файл или каталог

Подключить файл или каталог из компьютера запуска внутрь контейнера

```shell
docker container run --volume HOST_FILE:CONTAINER_FILE IMAGE
```

Псевдоним

```shell
docker container run -v HOST_FILE:CONTAINER_FILE IMAGE
```

Пример

```shell
docker container run --volume ./config/application.yaml:/app/config/application.yaml busybox
```

```shell
docker container run --volume ./config:/app/config/ busybox
```

### Добавить переменную окружения

```shell
docker container run --env KEY=VALUE IMAGE
```

Псевдоним

```shell
docker container run -e KEY=VALUE IMAGE
```

Пример

```shell
docker container run --env POSTGRES_PASSWORD=my_pass postgres:15
```

### Выполнение в фоновом режиме

```shell
docker container run --detach IMAGE
```

Псевдоним

```shell
docker container run -d IMAGE
```

Пример

```shell
docker container run --detach busybox
```

### Явно задать имя контейнеру

```shell
docker container run --name CONTAINER_NAME IMAGE
```

Пример

```shell
docker container run --name my_container busybox
```

### Автоматическое удаление контейнера после остановки

```shell
docker container run --rm IMAGE
```

Пример

```shell
docker container run --rm busybox
```

### Подключиться внутрь контейнера

```shell
docker container run -it IMAGE
```

```shell
docker container run --interactive --tty IMAGE
```

Пример

```shell
docker container run -it busybox
```

## Работа с Docker-образом

### Посмотреть список образов

```shell
docker image ls

REPOSITORY              TAG     IMAGE ID      CREATED       SIZE
alpine                  3.17    b2aa39c304c2  3 months ago  7.05MB
registry.k8s.io/busybox latest  e7d168d7db45  8 years ago   2.43MB
[---------------------] [----]  [----------]  [----------]  [----]
           |               |         |              |         |
           |               |         |              |         +--------> Размер образа
           |               |         |              +------------------> Время создания образа
           |               |         +---------------------------------> Уникальный идентификатор образа
           |               +-------------------------------------------> Версия/тег образа
           +-----------------------------------------------------------> Хост и имя образа
```

### Собрать образ из Dockerfile

```shell
docker image build [OPTIONS] PATH | URL | -
```

Псевдонимы

```shell
docker build [OPTIONS] PATH | URL | -
```

```shell
docker buildx build [OPTIONS] PATH | URL | -
```

```shell
docker buildx b [OPTIONS] PATH | URL | -
```

Пример

```shell
docker image build --tag my_app:1 .
```

```shell
docker image build --tag my_app:1 https://example.com/dockerfile
```

### Создать псевдоним образа

```shell
docker image tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
```

Псевдоним

```shell
docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
```

Пример

```shell
docker image tag my_app:1 docker.io/my_account/my_app:1
```

```shell
docker image tag my_app docker.io/my_account/my_app registry.k8s.io/my_app
```

### Удалить образ

```shell
docker image rm [OPTIONS] IMAGE [IMAGE...]
```

Псевдонимы

```shell
docker image remove [OPTIONS] IMAGE [IMAGE...]
```

```shell
docker rmi [OPTIONS] IMAGE [IMAGE...]
```

Пример

```shell
docker image rm my_app:1
```

```shell
docker image rm my_app docker.io/my_account/my_app
```

### Удалить все образы, не привязанные к контейнерам

```shell
docker image prune [OPTIONS]
```

Пример

```shell
docker image prune --force
```

### Сохранить образ в файл

```shell
docker image save --output FILE_NAME IMAGE [IMAGE...]
```

Псевдоним

```shell
docker save -o FILE_NAME IMAGE [IMAGE...]
```

Пример

```shell
docker image save --output my_app.tar my_app:1
```

### Загрузить образ из файла

```shell
docker image load --input FILE_NAME
```

Псевдоним

```shell
docker load --input FILE_NAME
```

Пример

```shell
docker image load --input my_app.tar
```

### Загрузить образ из удалённого хранилища

```shell
docker image pull [OPTIONS] NAME[:TAG|@DIGEST]
```

Псевдоним

```shell
docker pull [OPTIONS] NAME[:TAG|@DIGEST]
```

Пример

```shell
docker image pull alpine:3.17
```

```shell
docker image pull registry.k8s.io/busybox
```

### Отправить образ в удалённое хранилище

```shell
docker image push [OPTIONS] NAME[:TAG]
```

Псевдоним

```shell
docker push [OPTIONS] NAME[:TAG]
```

```shell
docker image push docker.io/my_account/my_app:1
```

## Авторизоваться в удалённом хранилище

### Авторизация

```shell
docker login [OPTIONS] [SERVER] [flags]
```

Пример

```shell
docker login registry-ci.delta.sbrf.ru
Username: TYZ-123
Password: ???????
```

```shell
docker login --username TYZ-123 --password ??????? registry-ci.delta.sbrf.ru
```

### Выход из учетной записи

```shell
docker logout [SERVER] [flags]
```

Пример

```shell
docker logout registry-ci.delta.sbrf.ru
```