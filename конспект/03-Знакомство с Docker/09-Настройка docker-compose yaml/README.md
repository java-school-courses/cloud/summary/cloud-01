Разберём из каких элементов состоит файл `docker-compose.yaml`

### Документация

Подробно про Docker-compose можете прочитать по ссылке [Docker-compose](https://docs.docker.com/compose/compose-file).

Про то, как составлять файл `docker-compose.yaml` по
ссылке [compose.yaml](https://docs.docker.com/compose/compose-file/05-services/).

### Верхнеуровневые секции

<table border="1"
       style="border-collapse: collapse; width: 100%; border-style: solid; border-color: rgba(0, 0, 0, 0.1); height: 132.53125px;">
    <tbody>
    <tr>
        <td style="text-align: center;"><h4>Секция</h4></td>
        <td><h4>Назначение</h4></td>
    </tr>
    <tr>
        <td style="text-align: center;">version</td>
        <td>Версия схемы</td>
    </tr>
    <tr>
        <td style="text-align: center;" rowspan="2">services</td>
        <td>Запускаемые контейнеры</td>
    </tr>
    <tr>
        <td>Основная секция, с которой будем работать</td>
    </tr>
    <tr>
        <td style="text-align: center;">networks</td>
        <td>Настройки сети</td>
    </tr>
    <tr>
        <td style="text-align: center;">volumes</td>
        <td>Постоянное хранилище</td>
    </tr>
    <tr>
        <td style="text-align: center;">configs</td>
        <td>Конфигурации приложения внутри контейнера</td>
    </tr>
    <tr>
        <td style="text-align: center;">secrets</td>
        <td>Чувствительные конфигурации приложения внутри контейнера</td>
    </tr>
    </tbody>
</table>

#### Пример

```yaml
version: '3.9'
services:
  db:
    image: bitnami/postgresql:15
    networks:
      - backend
    volumes:
      - data-db:/bitnami/postgresql
    configs:
      - source: init-db
        target: /docker-entrypoint-initdb.d/init-db.sql
    secrets:
      - source: server-certificate
        target: /cert/server.cert

networks:
  backend:

volumes:
  data-db:

configs:
  init-db:
    file: ./init-db.sql

secrets:
  server-certificate:
    file: ./server.cert
```

### Главные секции services

| Секция | Назначение |
|:------:|------------|
| image | Имя docker образа |
| ports | Прослушиваемые порты |
| environment | Переменные окружения |
| volumes | Постоянное хранилище |
| restart | Политика перезапуска |
| depends_on | Какой контейнер следует дождаться перед запуском |
| deploy | Параметры развертывания |

#### Пример

```yaml
version: '3.9'
services:
  db:
    image: bitnami/postgresql:15
    ports:
      - "5432:5432"
    environment:
      POSTGRESQL_PASSWORD: my_pass
    volumes:
      - ./db/:/docker-entrypoint-initdb.d/
    restart: always

  addressbook:
    image: addressbook:1
    ports:
      - "8080:8080"
    volumes:
      - ./configuration:/app/config
    depends_on:
      - db
    restart: always
```

### Главные параметры секции services.deploy

| Секция | Назначение |
|:------:|------------|
| replicas | Количество экземпляров приложения |
| labels | Метаданные о сервисе |
| resources | Выделенные ресурсы |

#### Пример

```yaml
version: '3.9'
services:
  addressbook:
    image: addressbook:1
    deploy:
      replicas: 2
      resources:
        limits:
          cpus: '0.50'
          memory: 500M
        reservations:
          cpus: '0.25'
          memory: 200M
      labels:
        app: addressbook
        language: java
```