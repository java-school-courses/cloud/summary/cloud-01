### Основные команды Docker-compose

Запускать команды в каталоге с `docker-compose.yaml` или `compose.yaml`

### Запустить контейнеры

```bash
docker-compose up
```

### Запустить контейнеры в фоновом режиме

```bash
docker-compose up --detach
```

### Остановить и очистить контейнеры

```bash
docker-compose down
```

### Показать список запущенных контейнеров

```bash
docker-compose ps
```

### Показать логи

```bash
docker-compose logs
```
