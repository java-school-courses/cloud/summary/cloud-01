Способ взаимодействия с OpenShift через интерфейс командной строки.

Для этого используется утилита `oc`, расшифровывается как OpenShift CLI.

### Где взять утилиту `oc`?

- В SberUser Soft. Искать по запросу "OpenShift".
- В OpenShift UI выбрать символ `?` в правом верхнем углу.
    - В раскрытом списке выбрать "Command Line Tools".
    - Скачать утилиту `oc` под вашу систему.
    - Распаковать архив.
    - Запустить как исполняемый файл.
      `IMAGE Command-line-tools.png`
- [На официальном сайте Red Hat](https://access.redhat.com/downloads/content/290). Для загрузки нужна регистрация.

### Документация

Утилита `oc` (OpenShift CLI) - расширяет функции утилиты `kubectl` (Kubernetes CLI).

Поэтому официальная документация Kubernetes подходит под наши задачи.

[Шпаргалка по kubectl](https://kubernetes.io/ru/docs/reference/kubectl/cheatsheet/)

### Алгоритм авторизации в OpenShift CLI

1) Авторизоваться в UI OpenShift;

2) В правом верхнем углу нажать на свой логин и выбрать пункт `Copy login command`;

3) Повторно авторизоваться в UI OpenShift;

4) Нажать на кнопку "Display Token";

5) Скопировать команду, которая начинается на `oc login --token=sha256~`;

6) Ввести в терминал ранее скопированную команду, которая начинается на `oc login --token=sha256~`.

### Базовые команды OpenShift CLI

#### Показать справку

```bash
oc COMMAND --help
```

Пример

```bash
oc get --help
```

#### Показать список ресурсов

```bash
oc get SUBJECT
```

Пример

```bash
oc get pods
```

#### Применить Manifest

```bash
oc apply --filename FILE_OR_DIR
```

```bash
oc apply -f FILE_OR_DIR
```

Пример

```bash
oc apply --filename pod.yaml
```

```bash
oc apply -f .
```

#### Удалить сущность

```bash
oc delete --filename FILE_OR_DIR
```

```bash
oc delete -f FILE_OR_DIR
```

Пример

```bash
oc delete --filename pod.yaml
```

```bash
oc delete -f .
```

#### Посмотреть логи

```bash
oc logs POD_NAME
```

Пример

```bash
oc logs addressbook-pod
```