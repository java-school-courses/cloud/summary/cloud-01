С помощью Spring Data подключим базу данных.

Для этого сделаем следующие действия:

- Создадим модель данных;
- Создадим repository;
- Подключим repository к controller;
- Пропишем атрибуты доступа к БД в application.yaml.

Код каждого шага представлен ниже.

#### Model

```java
package org.example.addressbook.model;

import org.springframework.data.annotation.Id;

import java.time.LocalDate;

public record AddressBook(@Id Long id, String firstName, String lastName, String phone, LocalDate birthday) {
}
```

#### Repository

```java
package org.example.addressbook.repository;

import org.example.addressbook.model.AddressBook;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface AddressBookRepository extends ReactiveCrudRepository<AddressBook, Long> {
}
```

#### Controller

```java
package org.example.addressbook.controller;

import org.example.addressbook.model.AddressBook;
import org.example.addressbook.repository.AddressBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1")
public class AddressBookController {
    final AddressBookRepository repository;

    @Autowired
    public AddressBookController(AddressBookRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/test")
    public String getTest() {
        return "Hello world";
    }

    @GetMapping("/reactive-test")
    public Mono<String> getReactiveTest() {
        return Mono.just("Hello reactive world");
    }

    @GetMapping("addressbooks")
    public Flux<AddressBook> getAddressBook() {
        return repository.findAll();
    }
}
```

#### application.yaml

```yaml
spring:
  r2dbc:
    url: r2dbc:h2:mem:///mydb
    username: sa
  jpa:
    database-platform: org.hibernate.dialect.H2Dialect
```