Spring Boot позволяет выполнить скрипты в файлах `schema.sql` и `data.sql`, если эти файлы лежат в каталоге ресурсов.

Добавим в `schema.sql` скрипт создания схемы базы данных.

А в `data.sql` скрипт добавления данных в БД.

#### schema.sql

```sql
DROP TABLE IF EXISTS ADDRESS_BOOK;
CREATE TABLE ADDRESS_BOOK
(
    ID         INT AUTO_INCREMENT PRIMARY KEY,
    FIRST_NAME VARCHAR(50) NOT NULL,
    LAST_NAME  VARCHAR(50) NOT NULL,
    PHONE      VARCHAR(20),
    BIRTHDAY   DATE
)
```

#### data.sql

```sql
INSERT INTO ADDRESS_BOOK
VALUES (-1, 'Aleksey', 'Alekseev', '+79000000000', '1980-01-01')
    INSERT
INTO ADDRESS_BOOK
VALUES (-2, 'Petr', 'Petrov', '+79111111111', '1990-01-01')
```

#### Controller

Так же, добавим в controller метод `save`, для сохранения новой записи в базу данных.

```java
package org.example.addressbook.controller;

import org.example.addressbook.model.AddressBook;
import org.example.addressbook.repository.AddressBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1")
public class AddressBookController {
    final AddressBookRepository repository;

    @Autowired
    public AddressBookController(AddressBookRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/test")
    public String getTest() {
        return "Hello world";
    }

    @GetMapping("/reactive-test")
    public Mono<String> getReactiveTest() {
        return Mono.just("Hello reactive world");
    }

    @GetMapping("addressbooks")
    public Flux<AddressBook> getAddressBook() {
        return repository.findAll();
    }

    @PostMapping("addressbook")
    public Mono<AddressBook> save(@RequestBody AddressBook addressBook) {
        return repository.save(addressBook);
    }
}
``` 