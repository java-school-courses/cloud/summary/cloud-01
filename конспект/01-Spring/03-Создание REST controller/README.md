Реализуем первую полезную функцию - ответ на HTTP запрос.

В начале сделаем обычный синхронный запрос.

Затем сделаем реактивный, то есть асинхронный запрос.

#### Код controller

```java
package org.example.addressbook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1")
public class AddressBookController {

    @GetMapping("/test")
    public String getTest() {
        return "Hello world";
    }

    @GetMapping("/reactive-test")
    public Mono<String> getReactiveTest() {
        return Mono.just("Hello reactive world");
    }
}
```

#### Полезные аннотации

Аннотации, для работы с HTTP запросами, которые будем использовать в дальнейшей работе:

| Название аннотации | Описание |
|:---:|:---:|
| `@RestController` | Сделать класс REST контроллером |
| `@RequestMapping` | Общие настройки для всех REST методов внутри класса |
| `@GetMapping` | Обработчик GET запроса |
| `@PostMapping` | Обработчик POST запроса |