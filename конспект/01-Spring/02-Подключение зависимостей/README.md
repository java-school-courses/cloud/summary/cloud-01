### Подключение зависимостей

#### Генератор Spring приложения

Зайдём на сайт [start.spring.io](https://start.spring.io).

Сгенерируем приложение с необходимыми зависимостями.

Скачаем архив.

Откроем заготовку приложения в IntelliJ IDEA.

#### Необходимые зависимости

Зависимости, которые нужно подключить в приложении:

- Spring Reactive Web
- Spring Data R2DBC
- H2 Database
- PostgreSQL Driver

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-webflux</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-r2dbc</artifactId>
    </dependency>

    <dependency>
        <groupId>com.h2database</groupId>
        <artifactId>h2</artifactId>
        <scope>runtime</scope>
    </dependency>
    <dependency>
        <groupId>io.r2dbc</groupId>
        <artifactId>r2dbc-h2</artifactId>
        <scope>runtime</scope>
    </dependency>
    <dependency>
        <groupId>org.postgresql</groupId>
        <artifactId>postgresql</artifactId>
        <scope>runtime</scope>
    </dependency>
    <dependency>
        <groupId>org.postgresql</groupId>
        <artifactId>r2dbc-postgresql</artifactId>
        <scope>runtime</scope>
    </dependency>
</dependencies>
```