Упакуем приложение в исполняемый jar файл. И запустим его.

#### Компиляция Jar файла

```bash
mvn clean package
```

#### Запуск приложения в Jar файле

```bash
java -jar target/addressbook-0.0.1-SNAPSHOT.jar
```